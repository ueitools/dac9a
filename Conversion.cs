﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UEI.QuickSet.DAC9a
{
    public class Conversion
    {
        public static List<byte> ConvertToLittleEndian(List<byte> byteData)
        {
            List<byte> _littelEndian = new List<byte>();
            for (int Len = byteData.Count - 1; Len >= 0; Len--)
            {
                _littelEndian.Add(byteData[Len]);
            }

            return _littelEndian;
        }

        public static byte[] ConvertToLittleEndian(byte[] byteData)
        {
            byte[] _littelEndian = new byte[byteData.Length];
            int counter=0;
            for (int Len = byteData.Length - 1; Len >= 0; Len--)
            {
                _littelEndian[counter] = byteData[Len];
                counter++;
            }
            
            return _littelEndian;
        }

        public static byte[] ConvertToLittleEndian(byte[] byteData,int byteLen)
        {
            byte[] _littelEndian = new byte[byteLen];
            byte[] _tempData = new byte[byteLen];

            if (byteData.Length < byteLen)
            {
                int len = byteLen - byteData.Length;
                int counter = byteLen - 1;
                for (int i = len - 1; i >= 0; i--)
                {
                    _tempData[counter]=(byte)0;
                }
                for (int j = 0; j < byteData.Length; j++)
                {
                    _tempData[j] = byteData[j];
                }
            }
            int _counter = 0;
            for (int Len = _tempData.Length - 1; Len >= 0; Len--)
            {
                _littelEndian[_counter] = byteData[Len];
                _counter++;
            }

            return _littelEndian;
        }
        
        public static byte[] ConvertTo4ByteLittleEndian(int intData)
        {
            byte[] _littleEndian = new byte[4];
            _littleEndian[3] = (byte)((intData >> 24) & 0xff);
            _littleEndian[2] = (byte)((intData >> 16) & 0xff);
            _littleEndian[1] = (byte)((intData >> 8) & 0xff);
            _littleEndian[0] = (byte)(intData & 0xff); 

            return _littleEndian;
        }
        public static byte[] ConvertTo3ByteLittleEndian(int intData)
        {
            byte[] _littleEndian = new byte[3];
            
            _littleEndian[2] = (byte)((intData >> 16) & 0xff);
            _littleEndian[1] = (byte)((intData >> 8) & 0xff);
            _littleEndian[0] = (byte)(intData & 0xff); 
            return _littleEndian;
        }
        public static byte[] ConvertTo2ByteLittleEndian(int intData)
        {
            byte[] _littleEndian = new byte[2];
            _littleEndian[1] = (byte)((intData >> 8) & 0xff);
            _littleEndian[0] = (byte)(intData & 0xff);
            return _littleEndian;
        }
        //public static byte[] ConvertToByte(int _data,int byteSize)
        //{
        //    byte[] _retData = new byte[byteSize];

        //    _retData=ConvertToByte(

        //    //try
        //    //{
        //    //    //byte[] _tempData = Encoding.ASCII.GetBytes(_data.ToString());
        //    //    byte[] _tempData = ConvertToByte(_data);
        //    //    byte[] _newTempData = new byte[byteSize];
        //    //    if (_tempData.Length > byteSize)
        //    //    {
        //    //        for(int size=0;size<byteSize;size++)
        //    //        {
        //    //            _newTempData[size] = _tempData[size];
        //    //        }
        //    //        _tempData = new byte[byteSize];
        //    //        _tempData = _newTempData;
        //    //    }

        //    //    for (int i = 0; i < _tempData.Length; i++)
        //    //    {
        //    //        _retData[i] = _tempData[i];
        //    //    }

        //    //    if (_retData.Length < byteSize)
        //    //    {
        //    //        int len = byteSize - _retData.Length;
        //    //        for (int i = len; i > 0; i--)
        //    //        {
        //    //            _retData[i] = (byte)0;
        //    //        }

        //    //    }
                
        //        return _retData;
        //    //}
        //    //catch (Exception ex)
        //    //{
        //    //    throw ex;
        //    //}
        //}

        public static byte[] ConvertToByte(int _data)
        {
            
            return BitConverter.GetBytes(_data);

        }

        public static byte[] ConvertToByte(int data, int bytesize )
        {
            byte[] retdata = new byte[bytesize];

            switch (bytesize)
            {
                //case 1:
                //    {
                //        retdata[0] = (byte)(data & 0xff);
                //        break;
                //    }
                //case 2:
                //    {
                //        retdata[0] = (byte)(data & 0xff);
                //        retdata[1] = (byte)((data >> 8) & 0xff);
                //        break;
                //    }
                //case 3:
                //    {
                //        retdata[0] = (byte)(data & 0xff);
                //        retdata[1] = (byte)((data >> 8) & 0xff);
                //        retdata[2] = (byte)((data >> 16) & 0xff);
                //        break;
                //    }
                //case 4:
                //    {
                //        retdata[0] = (byte)(data & 0xff);
                //        retdata[1] = (byte)((data >> 8) & 0xff);
                //        retdata[2] = (byte)((data >> 16) & 0xff);
                //        retdata[3] = (byte)((data >> 24) & 0xff);
                //        break;
                //    }
                case 1:
                    {
                        retdata[0] = (byte)(data & 0xff);
                        break;
                    }
                case 2:
                    {
                        retdata[1] = (byte)(data & 0xff);
                        retdata[0] = (byte)((data >> 8) & 0xff);
                        break;
                    }
                case 3:
                    {
                        retdata[2] = (byte)(data & 0xff);
                        retdata[1] = (byte)((data >> 8) & 0xff);
                        retdata[0] = (byte)((data >> 16) & 0xff);
                        break;
                    }
                case 4:
                    {
                        retdata[3] = (byte)(data & 0xff);
                        retdata[2] = (byte)((data >> 8) & 0xff);
                        retdata[1] = (byte)((data >> 16) & 0xff);
                        retdata[0] = (byte)((data >> 24) & 0xff);
                        break;
                    }
            }

            return retdata;
        }

        public static byte[] ConvertToByte(long data, int bytesize)
        {
            byte[] retdata = new byte[bytesize];

            switch (bytesize)
            {
                //case 1:
                //    {
                //        retdata[0] = (byte)(data & 0xff);
                //        break;
                //    }
                //case 2:
                //    {
                //        retdata[0] = (byte)(data & 0xff);
                //        retdata[1] = (byte)((data >> 8) & 0xff);
                //        break;
                //    }
                //case 3:
                //    {
                //        retdata[0] = (byte)(data & 0xff);
                //        retdata[1] = (byte)((data >> 8) & 0xff);
                //        retdata[2] = (byte)((data >> 16) & 0xff);
                //        break;
                //    }
                //case 4:
                //    {
                //        retdata[0] = (byte)(data & 0xff);
                //        retdata[1] = (byte)((data >> 8) & 0xff);
                //        retdata[2] = (byte)((data >> 16) & 0xff);
                //        retdata[3] = (byte)((data >> 24) & 0xff);
                //        break;
                //    }
                case 1:
                    {
                        retdata[0] = (byte)(data & 0xff);
                        break;
                    }
                case 2:
                    {
                        retdata[1] = (byte)(data & 0xff);
                        retdata[0] = (byte)((data >> 8) & 0xff);
                        break;
                    }
                case 3:
                    {
                        retdata[2] = (byte)(data & 0xff);
                        retdata[1] = (byte)((data >> 8) & 0xff);
                        retdata[0] = (byte)((data >> 16) & 0xff);
                        break;
                    }
                case 4:
                    {
                        retdata[3] = (byte)(data & 0xff);
                        retdata[2] = (byte)((data >> 8) & 0xff);
                        retdata[1] = (byte)((data >> 16) & 0xff);
                        retdata[0] = (byte)((data >> 24) & 0xff);
                        break;
                    }
            }

            return retdata;
        }

        public static byte[] ConvertToByte(string _data, int byteSize)
        {
            byte[] _retData = new byte[byteSize];

            try
            {
                _retData = Encoding.ASCII.GetBytes(_data.Trim());


                return _retData;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static byte[] ConvertToByte(string hexdata)
        {
            byte[] _retData = new byte[1];

            try
            {
                _retData[0] = (byte)Convert.ToInt16(hexdata, 16);


                return _retData;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static byte[] ConvertUTF16ToUTF8(string utf16data)
        {
            byte[] utf16Bytes = Encoding.Unicode.GetBytes(utf16data);
            byte[] utf8Bytes = Encoding.Convert(Encoding.Unicode, Encoding.UTF8, utf16Bytes);

            return utf8Bytes;
        }

        public static byte[] ConvertUTF32ToUTF8(string utf32data)
        {
            byte[] utf32Bytes = Encoding.Unicode.GetBytes(utf32data);
            byte[] utf8Bytes = Encoding.Convert(Encoding.Unicode, Encoding.UTF8, utf32Bytes);

            return utf8Bytes;
        }

        public static byte[] ConvertUTF8ToUTF16(string utf8data)
        {
            //UTF8 bytes
            byte[] utf8Bytes = Encoding.UTF8.GetBytes(utf8data);

            //Converting to Unicode from UTF8 bytes
            
            byte[] unicodeBytes = Encoding.Convert(Encoding.UTF8, Encoding.Unicode, utf8Bytes);

            return unicodeBytes;
        }

        public static string ConvertByteArrayToUTF8(byte[] data)
        {
           string _data=Encoding.UTF8.GetString(data);
           return _data;
        }

        //public static string ConvertByteArrayToISO3166(byte[] data)
        //{
        //    Encoding isoEnc = Encoding.GetEncoding("iso-8859-1");
        //    string utfResult = Encoding.UTF8.GetString(isoEnc.GetBytes(myNode.InnerText));
        //}

        public static byte[] ConvertISO3166toByte(string data)
        {
            Encoding isoEnc = Encoding.GetEncoding("iso 3166-1");
            return isoEnc.GetBytes(data);
        }

        //public static string ConvertByteArrayToISO3166(byte[] data)
        //{
        //    Encoding isoEnc = Encoding.GetEncoding("iso 3166-1");
        //    string _data = Encoding.UTF8.GetString();

            
        //}

        public static string ConvertByteToString(byte[] data)
        {
            return Encoding.ASCII.GetString(data);
        }

        public static int ConvertByteArrayToInt32(byte[] data)
        {
           
            if (data.Length == 3)
            {
                byte[] _temp = new byte[4];
                _temp[3] = 0x00;
                _temp[2] = data[2];
                _temp[1] = data[1];
                _temp[0] = data[0];
                data = _temp;

            }

            if (data.Length == 2)
            {
                byte[] _temp = new byte[4];
                _temp[3] = 0x00;
                _temp[2] = 0x00;
                _temp[1] = data[1];
                _temp[0] = data[0];
                data = _temp;

            }
            return BitConverter.ToInt32(data, 0);

           
        }

        public static int ConvertByteArrayToInt16(byte[] data)
        {
            return BitConverter.ToInt16(data, 0);
          
        }

        public static int ConvertBytetoInt(byte data)
        {
            return (int)data;

        }

        public static List<byte> ConvertCodesetToByte(string id)
        {
            List<byte> _data = new List<byte>();
            
            foreach (byte by in Conversion.ConvertToLittleEndian(Conversion.ConvertToByte(int.Parse(id.Trim().Substring(1, 4)), 2)))
            {
                _data.Add(by);
            }
            _data.Add(Conversion.ConvertToByte(id.Trim().Substring(0, 1), 1)[0]);
            return _data;
        }

        public static string GetArdKey(string id, string priority)
        {
            string _ard = string.Empty;
            string _mode = Conversion.ConvertToByte(id.Trim().Substring(0, 1),1)[0].ToString("X");
            byte[] _bid = Conversion.ConvertToByte(int.Parse(id.Trim().Substring(1, 4)), 2);
            string _id = string.Empty;
            foreach (byte by in _bid)
            {
                string _temp = by.ToString("X");
                if (_temp.Length == 1)
                {
                    _id += "0";
                }
                _id += by.ToString("X");
            }
            byte[] _priority = Conversion.ConvertToByte(int.Parse(priority.Trim()), 1);
            string _pr = string.Empty;
            foreach (byte by in _priority)
            {
                string _temp = by.ToString("X");
                if (_temp.Length == 1)
                {
                    _pr += "0";
                }
                _pr += by.ToString("X");
            }
            _ard = "0x" + _mode + _id + _pr;
            return _ard;
        }

        public List<byte> GetByteArdKey(string ard)
        {
            List<byte> _ard = new List<byte>();

            return _ard;
        }

        public List<byte> GetLitteEndianArdKey(List<byte> ard)
        {
            List<byte> _ard = new List<byte>();

            return _ard;
        }


    }
}
