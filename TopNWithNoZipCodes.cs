﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UEI.QuickSet.DAC9a
{
    public class TopNWithNoZipCodes
    {
        public TypeRecordHeader _typerecheader;

        public TopNWithNoZipCodes()
        {
            _typerecheader = new TypeRecordHeader();
        }

        public class TypeRecordHeader
        {
            public string _typeRecordHeaderId;
            public int _noTypeRecords;
            public List<TypeRecord> _typeRecords;

            public TypeRecordHeader()
            {
                _typeRecordHeaderId = string.Empty;
                _noTypeRecords = 0;
                _typeRecords = new List<TypeRecord>();
            }

        }
        public class TypeRecord
        {
            public int _typeNameLen;
            public string _typeName;
            public int _osmNameLen;
            public string _osmName;
            public int _typeLocationHashDataOffset;
            TypeLocationHashData _locationHashData;

            public TypeRecord()
            {
                _typeNameLen = 0;
                _typeName = string.Empty;
                _osmNameLen = 0;
                _osmName = string.Empty;
                _typeLocationHashDataOffset = 0;
                _locationHashData = new TypeLocationHashData();
            }

        }
        public class TypeLocationHashData
        {
            public int _locationHashCount;
            public List<HashValueOffset> _locationHashValueOffset;

            public class HashValueOffset
            {
                public string _locationValue;
                public int _locationFileOffset;
                public List<LocationRecord> _locationRecords;

                public HashValueOffset()
                {
                    _locationValue = string.Empty;
                    _locationFileOffset = 0;
                    _locationRecords = new List<LocationRecord>();
                }
            }

            public TypeLocationHashData()
            {
                _locationHashCount = 0;
                _locationHashValueOffset = new List<HashValueOffset>();

            }
        }
        public class LocationRecord
        {
            public string _locationData;
            public int _brandCount;
            public int _brandIndex;
            public int _codesetCount;
            public List<string> _codesets;

            public LocationRecord()
            {
                _locationData = string.Empty;
                _brandCount = 0;
                _brandIndex = 0;
                _codesetCount = 0;
                _codesets = new List<string>();
            }
        }



    }
}
