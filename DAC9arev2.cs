﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Text;
using System.Data;

namespace UEI.QuickSet.DAC9a
{
    //DAC9a Codeset format. This is used only for DAC9a may be need to be changed - 08/30/2014
    //T2051- 03 08 54
    //byte 2 - 03
    //byte 1 - 08
    //byte 0 - 54
    public class DAC9aData
    {
        ByteType _bType;

        public ByteType BType
        {
            get { return _bType; }
            set { _bType = value; }
        }
        FileHeader _dac9aFileHeader;
        HuffmanTree _dac9aHuffmanTree;
        BrandRecordHeader _dac9aBrandRecordHeader;
        BrandCollections _dac9aBrandRecords;
        TypeRecordHeader _dac9aTypeRecordHeader;
        TypeRecordHeader.TypeRecord _dac9aTypeRecord;
        
        TypeLocationHashData _dac9aTypeLocationhashData;
        List<LocationRecordCollection> _dac9aLocationRecords;

        //TypeLocationHashData _dac9aTypeProvinceCityData;
        //List<LocationRecordCollection> _dac9aProvinceCityLocationRecords;
        DataTable _dac9apcLocationHashData;
        DataTable _dac9apclocdt;
        List<PCTypeLocationHashData> _dac9apctypelochasdatacollection;
        List<PCLocationData> _dac9apctypelocdatacollection;
        
        
        ProviderLocationHashData _dac9aProviderLocationhashData;
        ProviderLocationCollection _dac9aProviderLocationCollection;

        ProviderLocationHashDataNoZipCodes _dac9aProviderLocationhashDataWithNoZipCodes;
        ProviderLocationCollectionNoZipCodes _dac9aProviderLocationCollectionNoZipCodes;

        List<ProviderLocationHashDataNoZipCodes> _dac9aProviderLocationhashDataWithNoZipCodesCollection=new List<ProviderLocationHashDataNoZipCodes>();
        List<ProviderLocationCollectionNoZipCodes> _dac9aProviderLocationCollectionNoZipCodesCollection=new List<ProviderLocationCollectionNoZipCodes>();

        ProviderLocationHashDataNoZipCodes _dac9aProviderLocationhashDataTopNManufacturers;
        ProviderLocationCollectionNoZipCodes _dac9aProviderLocationCollectionTopNManufacturers;

        List<ProviderLocationHashDataNoZipCodes> _dac9aProviderLocationhashDataWithTopNManufacturersCollection = new List<ProviderLocationHashDataNoZipCodes>();
        List<ProviderLocationCollectionNoZipCodes> _dac9aProviderLocationCollectionTopNManufacturersCollection = new List<ProviderLocationCollectionNoZipCodes>();

        bool _isZipcodesAvailable = false;

        public bool IsZipcodesAvailable
        {
            get { return _isZipcodesAvailable; }
            set { _isZipcodesAvailable = value; }
        }
        bool _isProvidersAvailable = false;

        public bool IsProvidersAvailable
        {
            get { return _isProvidersAvailable; }
            set { _isProvidersAvailable = value; }
        }

        int _stAddress, _edAddress=0;

        int _pageSize = 0;
        public int PageSize
        {
            get { return _pageSize; }
            set { _pageSize = value; }
        }
        
        public void SetFileHeader(string filetype,int fileversion,int fileflag)
        {
            
            _dac9aFileHeader.FileType = filetype.Trim();
            _dac9aFileHeader.FileVersion = fileversion;
            _dac9aFileHeader.FileFlags = fileflag;
            _dac9aFileHeader.ConvertToByte();
            _dac9aFileHeader.ConvertToEndian(BType);
            
        }
        public void SetHuffmanTree(int treeLen)
        {
           
            _dac9aHuffmanTree.TreeLen=0;
            _dac9aHuffmanTree.ConvertToByte();
            _dac9aHuffmanTree.ConvertToEndian(BType);
            
        }
        public void SetBrandRecord(List<string> brands)
        {
            
            _dac9aBrandRecords.Brands = new List<BrandRecord>();
            
            BrandRecord _brnd = new BrandRecord();
            foreach (string brand in brands)
            {
                
                _brnd = new BrandRecord();
                _brnd.BrandName = brand;
                _brnd.ConvertToByte();
                _brnd.ConvertToEndian(BType);
                _brnd.RecordSize = _brnd.ByteBrandName.Length;
                _brnd.ConvertToByte();
                _brnd.ConvertToEndian(BType);
                
                _dac9aBrandRecords.Brands.Add(_brnd);
            }

        }
        private int GetBrandRecordSize()
        {
            int _size = 0;
            foreach (BrandRecord br in _dac9aBrandRecords.Brands)
            {
                _size += br.ByteRecordSize.Length;
                _size += br.ByteBrandName.Length;
            }
            return _size;
        }
        public void SetBrandRecordHeader(string header)
        {
            
            _dac9aBrandRecordHeader.BrandHeader = header;
            _dac9aBrandRecordHeader.BrandRecordSize = GetBrandRecordSize();
            _dac9aBrandRecordHeader.BrandsCount = _dac9aBrandRecords.Brands.Count;

            _dac9aBrandRecordHeader.ConvertToByte();
            _dac9aBrandRecordHeader.ConvertToEndian(BType);
            
        }
        public void SetTypeRecordHeader(string header,int noheaders,List<TypeRecordHeader.TypeRecord> typerecs)
        {
            
            _dac9aTypeRecordHeader.TypeRecHeader = "TR";
            _dac9aTypeRecordHeader.NoTypeRecords = noheaders;
            //_dac9aTypeRecordHeader.TypeRecord = typerecords;
            _dac9aTypeRecordHeader.TypeRecords = typerecs;
            _dac9aTypeRecordHeader.ConvertToByte();
            _dac9aTypeRecordHeader.ConvertToEndian(BType);
            
            
        }
        public TypeRecordHeader.TypeRecord SetTypeRecord(string typename, string osmname)
        {
            _dac9aTypeRecord = new TypeRecordHeader.TypeRecord();
            _dac9aTypeRecord.Typename = typename;
            _dac9aTypeRecord.TypeNameLen = typename.Trim().Length;
            _dac9aTypeRecord.OsmName = osmname;
            _dac9aTypeRecord.OsmNamelen = osmname.Trim().Length;
            _dac9aTypeRecord.TypeLocHashDataFileOffset = 0;
            _dac9aTypeRecord.ConvertToByte();
            _dac9aTypeRecord.ConvertToEndian(BType);
            
            return _dac9aTypeRecord;
        }
        public void SetProvinceAncCityTypeLocationHashData(DataTable _locationhash)
        {
            _dac9apcLocationHashData = new DataTable();
            _dac9apcLocationHashData = _locationhash;
            _dac9apctypelochasdatacollection = new List<PCTypeLocationHashData>();
            
            foreach (DataRow dr in _locationhash.Rows)
            {

                PCTypeLocationHashData _hashdata = new PCTypeLocationHashData();
                _hashdata.Countrycode=Convert.FromBase64String(dr.ItemArray[0].ToString());
                byte[] _prcode = Convert.FromBase64String(dr.ItemArray[1].ToString());
                //string _pcode = Encoding.ASCII.GetString(_prcode);
                _hashdata.Provincecode = Conversion.ConvertToLittleEndian(_prcode, 2);
                _hashdata.FileOffset = Convert.FromBase64String(dr.ItemArray[2].ToString());//This should be a 4 byte size
                _dac9apctypelochasdatacollection.Add(_hashdata);
            }

        }
        public void SetProviceAndCityLocationData(DataTable _brandcount,DataTable _branddetails)
        {
            _dac9apclocdt = new DataTable();
            _dac9apctypelocdatacollection = new List<PCLocationData>();
            int rowcounter = 0;
            try
            {
                foreach (DataRow dr1 in _brandcount.Rows)
                {
                    PCLocationData _pclocdt = new PCLocationData();
                    _pclocdt = ConvertpclochashToByteStream(dr1.ItemArray);
                    _pclocdt.BrandIdCollection = new List<PCBrandIdCollection>();
                    for (int i = rowcounter; i < (rowcounter + int.Parse(dr1.ItemArray[2].ToString())); i++)
                    {
                        object[] _rowdata = _branddetails.Rows[i].ItemArray;
                        _pclocdt.BrandIdCollection.Add(ConvertpclocdattoByteStream(_rowdata));
                    }
                    rowcounter += int.Parse(dr1.ItemArray[2].ToString());
                    _dac9apctypelocdatacollection.Add(_pclocdt);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private string GetProvince(string data)
        {
            string _province=string.Empty;
            int len = data.Length;
            switch (len)
            {
                case 8:
                    {
                        _province = data.Substring(0, 4);
                        break;
                    }
                case 7:
                    {
                        _province = data.Substring(0, 3);
                        break;
                    }
                case 6:
                    {
                        _province = data.Substring(0, 2);
                        break;
                    }
                case 5:
                    {
                        _province = data.Substring(0, 1);
                        break;
                    }
                case 4:
                    {
                        break;
                    }
                case 3:
                    {
                        break;
                    }
                case 2:
                    {
                        break;
                    }
                case 1:
                    {
                        break;
                    }
            }
            return _province;
        }

        public PCBrandIdCollection ConvertpclocdattoByteStream(object[] _data)
        {
            PCBrandIdCollection _bridcollection = new PCBrandIdCollection();
            _bridcollection.Brandindex = Conversion.ConvertToLittleEndian(Conversion.ConvertToByte(int.Parse(GetBrandIndex(_data[2].ToString())), 2), 2);
            _bridcollection.Cscount = Conversion.ConvertToLittleEndian(Conversion.ConvertToByte(int.Parse(_data[3].ToString()), 1));
            List<byte> idlist = new List<byte>();
            string[] sep = { "," };
            string[] _ids = _data[4].ToString().Trim().Split(sep, StringSplitOptions.RemoveEmptyEntries);
            foreach (string id in _ids)
            {
                string _mode = id.Substring(0, 1);
                string _setupcode = id.Substring(1, 4);
               
                byte[] _scode = Conversion.ConvertToLittleEndian(Conversion.ConvertToByte(int.Parse(_setupcode), 2), 2);
                foreach (byte by in _scode)
                {
                    idlist.Add(by);
                }
                idlist.Add(Conversion.ConvertToByte(_mode, 1)[0]);//This was changed based on the input from Rex need to be evaluated
            }
            _bridcollection.Cslist = idlist.ToArray();
            return _bridcollection;
        }

        public PCLocationData ConvertpclochashToByteStream(object[] _data)
        {
            PCLocationData _pclocdata = new PCLocationData();
            //_pclocdata.Locationdata = Conversion.ConvertToByte(_data[0].ToString(), 4);
            //string _pr=string.Empty;
            //string _cty=string.Empty;
            //string _location = string.Empty;

            //if (_data[0].ToString().Length == 4)
            //{
            //    _pr = _data[0].ToString().Substring(0, 2);
            //    _cty = _data[0].ToString().Substring(2, 2);
            //    _location = _pr.ToString().Trim() + _cty.ToString().Trim();
            //}
            //else if (_data[0].ToString().Length == 6)
            //{
            //    _pr = _data[0].ToString().Substring(0, 2);
            //    _cty = _data[0].ToString().Substring(2, 4);
            //    _location = _pr.ToString().Trim() + Convert.ToInt32(_cty,16).ToString();

            //}
            //else if (_data[0].ToString().Length == 3)
            //{
            //    _pr = _data[0].ToString().Substring(0, 2);
            //    _cty = _data[0].ToString().Substring(2, 1);
            //    _location = _pr.ToString().Trim() + Convert.ToInt32(_cty, 16).ToString();
            //}

            Int32 _prv=Convert.ToInt32(_data[0].ToString().Trim(),16);

            string _cty = _data[1].ToString().Trim();
            string _loc=string.Empty;
            switch (_cty.Length)
            {
                case 1:
                    {
                        _loc = _data[0].ToString().Trim() + "000" + _cty;
                        break;
                    }
                case 2:
                    {
                        _loc = _data[0].ToString().Trim() + "00" + _cty;
                        break;
                    }
                case 3:
                    {
                        _loc = _data[0].ToString().Trim() + "0" + _cty;
                        break;
                    }
                case 4:
                    {
                        _loc = _data[0].ToString().Trim() + _cty;
                        break;
                    }
            }
            //Int32 _city=Convert.ToInt32(_data[1].ToString().Trim(),16);

            long _location = Convert.ToInt32(_loc, 16);

            _pclocdata.Locationdata = Conversion.ConvertToLittleEndian(Conversion.ConvertToByte(_location, 4), 4);
            _pclocdata.Brandcount = Conversion.ConvertToLittleEndian(Conversion.ConvertToByte(int.Parse(_data[2].ToString()), 1));
            
            
            return _pclocdata;
        }

        
        public void SetLocationRecord(Dictionary<int, Dictionary<string, List<string>>> zipcollections)
        {
            int counter = 0;
           
            LocationRecord _locRec = new LocationRecord();
            LocationRecordCollection _locCollection = new LocationRecordCollection();
            _dac9aTypeLocationhashData = new TypeLocationHashData();
            _dac9aTypeLocationhashData.LocationCollection = new List<TypeLocationHashData.HashValues>();
            TypeLocationHashData.HashValues _hashcollection = new TypeLocationHashData.HashValues();

            int _pgLen = ((int)((double)zipcollections.Count /(double) PageSize)); ;
            long result = 0;
            Math.DivRem((long)zipcollections.Count, (long)PageSize, out result);
            if (result!=0)
            {
                _pgLen++;
            }
                        
            _dac9aTypeLocationhashData.LocationHashCount = (int)_pgLen;
            foreach (KeyValuePair<int, Dictionary<string, List<string>>> _zip in zipcollections)
            {
                _stAddress = _edAddress + 1;
                if (counter < PageSize)
                {
                    _locRec.LocationData = _zip.Key;
                    _locRec.BrandCount = _zip.Value.Count;
                   _locRec.BrandIdCollection = new List<LocationRecord.BrandIds>();
                    LocationRecord.BrandIds _brandIds = new LocationRecord.BrandIds();
                    foreach (KeyValuePair<string, List<string>> _brands in _zip.Value)
                    {
                        _brandIds.BrandIndex = int.Parse(GetBrandIndex(_brands.Key.ToString().Trim()));
                        _brandIds.CodesetCount = _brands.Value.Count;
                        _brandIds.Codesets = new List<string>();
                        foreach (string id in _brands.Value)
                        {
                            _brandIds.Codesets.Add(id);
                            
                        }
                        _brandIds.ConvertToByte();
                        _brandIds.ConvertToEndian(BType);

                        _locRec.BrandIdCollection.Add(_brandIds);

                        _brandIds = new LocationRecord.BrandIds();
                        
                    }
                    _locRec.ConvertToByte();
                    _locRec.ConvertToEndian(BType);
                    _edAddress = _locRec.GetEndAddress(_stAddress);
                    _locCollection.LocRecords.Add(_locRec);
                    _locRec = new LocationRecord();
                    
                    counter++;
                }
                else
                {
                    //Add it to the new location hash value and hash offset
                    _dac9aLocationRecords.Add(_locCollection);
                    _locCollection = new LocationRecordCollection();

                    _hashcollection.LocationHashValue = 0;
                    _hashcollection.LocationFileOffset = 0;
                    _dac9aTypeLocationhashData.LocationCollection.Add(_hashcollection);
                   
                    _hashcollection = new TypeLocationHashData.HashValues();
                    
                    counter = 0;
                    _locRec = new LocationRecord();
                    _locRec.LocationData = _zip.Key;
                    _locRec.BrandCount = _zip.Value.Count;
                    _locRec.BrandIdCollection = new List<LocationRecord.BrandIds>();
                    LocationRecord.BrandIds _brandIds = new LocationRecord.BrandIds();
                    foreach (KeyValuePair<string, List<string>> _brands in _zip.Value)
                    {
                        _brandIds.BrandIndex = int.Parse(GetBrandIndex(_brands.Key.ToString().Trim()));
                        _brandIds.CodesetCount = _brands.Value.Count;
                        _brandIds.Codesets = new List<string>();
                       
                        foreach (string id in _brands.Value)
                        {
                            _brandIds.Codesets.Add(id);

                        }
                        _brandIds.ConvertToByte();
                        _brandIds.ConvertToEndian(BType);

                        _locRec.BrandIdCollection.Add(_brandIds);

                        _brandIds = new LocationRecord.BrandIds();

                    }
                    _locRec.ConvertToByte();
                    _locRec.ConvertToEndian(BType);
                    _edAddress = _locRec.GetEndAddress(_stAddress);
                    _locCollection.LocRecords.Add(_locRec);
                    _locRec = new LocationRecord();
                   

                    counter++;
                }
                
                
            }

            if (_locCollection.LocRecords.Count > 0)
            {
                _hashcollection.LocationHashValue = 0;
                _hashcollection.LocationFileOffset = 0;
                _dac9aTypeLocationhashData.LocationCollection.Add(_hashcollection);
                _dac9aLocationRecords.Add(_locCollection);
            }

            _dac9aTypeLocationhashData.ConvertToByte();
            _dac9aTypeLocationhashData.ConvertToEndian(BType);
            _edAddress = _dac9aTypeLocationhashData.GetEndAddress(_stAddress);

        }
        //Added on 11/22/2013 to handle the location record without the zip code
        public void SetProviderRecord(List<Locations> _locations,string provider)
        {
            _dac9aProviderLocationhashDataWithNoZipCodes = new ProviderLocationHashDataNoZipCodes();
            _dac9aProviderLocationhashDataWithNoZipCodes.ProviderName = provider;
            _dac9aProviderLocationhashDataWithNoZipCodes.LocationHashCount = _locations.Count;
            _dac9aProviderLocationhashDataWithNoZipCodes.LocationHashCollection = new List<ProviderLocationHashDataNoZipCodes.ProviderLocationHash>();
            ProviderLocationHashDataNoZipCodes.ProviderLocationHash _temp = new ProviderLocationHashDataNoZipCodes.ProviderLocationHash();
            for (int i = 0; i < _locations.Count; i++)
            {
                _temp = new ProviderLocationHashDataNoZipCodes.ProviderLocationHash();
                _temp.LocationHashValues = _locations[i].Region + _locations[i].Country;
                _temp.LocationHashOffset = 0;
                _temp.ConvertToByte();
                _temp.ConvertToLittleEndian();
                _dac9aProviderLocationhashDataWithNoZipCodes.LocationHashCollection.Add(_temp);
            }
            _dac9aProviderLocationhashDataWithNoZipCodes.ConvertToByte();
            _dac9aProviderLocationhashDataWithNoZipCodes.ConvertToLittleEndian();

            _dac9aProviderLocationhashDataWithNoZipCodesCollection.Add(_dac9aProviderLocationhashDataWithNoZipCodes);
        }
        public void SetProviderLocationRecord(List<Locations> _locations,string provider)
        {
            _dac9aProviderLocationCollectionNoZipCodes = new ProviderLocationCollectionNoZipCodes();
            _dac9aProviderLocationCollectionNoZipCodes.ProviderList = new List<ProviderLocationRecordNoZipCodes>();
            _dac9aProviderLocationCollectionNoZipCodes.ProviderName = provider;
            ProviderLocationRecordNoZipCodes _providerLocRecord = new ProviderLocationRecordNoZipCodes();
            try
            {
                for (int i = 0; i < _locations.Count; i++)
                {
                    _providerLocRecord = new ProviderLocationRecordNoZipCodes();
                    _providerLocRecord.LocationData = _locations[i].Region + _locations[i].Country;
                    _providerLocRecord.ProviderLocRec = new List<ProviderLocationRecordNoZipCodes.LocationRecords>();
                    _providerLocRecord.BrandCount = _locations[i].Brandlist.Count;
                    ProviderLocationRecordNoZipCodes.LocationRecords _tempData;
                    foreach (Brands _brand in _locations[i].Brandlist)
                    {
                        _tempData = new ProviderLocationRecordNoZipCodes.LocationRecords();
                        _tempData.BrandIndex = int.Parse(GetBrandIndex(_brand.Name));
                        _tempData.CodesetCount = _brand.Idlist.Count;

                        List<string> idlist = new List<string>();
                        foreach (string _id in _brand.Idlist)
                        {
                            if (!string.IsNullOrEmpty(_id.Trim()))
                            {
                                idlist.Add(_id);
                            }
                        }

                        //_tempData.Codesets = _brand.Idlist;
                        _tempData.Codesets = idlist;
                        _tempData.ConvertToByte();
                        _tempData.ConvertToLittleEndian();
                        _providerLocRecord.ProviderLocRec.Add(_tempData);

                    }
                    _providerLocRecord.ConvertToByte();
                    _providerLocRecord.ConvertToLittleEndian();
                    _dac9aProviderLocationCollectionNoZipCodes.ProviderList.Add(_providerLocRecord);
                }
                _dac9aProviderLocationCollectionNoZipCodesCollection.Add(_dac9aProviderLocationCollectionNoZipCodes);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public void SetProviderRecord(Dictionary<string, List<string>> hashcollection)
        {
            _dac9aProviderLocationhashData=new ProviderLocationHashData();
            _dac9aProviderLocationhashData.LocationHashCount = hashcollection.Count;
            _dac9aProviderLocationhashData.LocationHashCollection = new List<ProviderLocationHashData.ProviderLocationHash>();
            ProviderLocationHashData.ProviderLocationHash _temp = new ProviderLocationHashData.ProviderLocationHash();
            foreach (KeyValuePair<string, List<string>> _hash in hashcollection)
            {
                _temp = new ProviderLocationHashData.ProviderLocationHash();
                _temp.LocationHashValues = _hash.Key;
                _temp.LocationHashOffset = 0;
                _temp.ConvertToByte();
                _temp.ConvertToLittleEndian();
                _dac9aProviderLocationhashData.LocationHashCollection.Add(_temp);
            }

            _dac9aProviderLocationhashData.ConvertToByte();
            _dac9aProviderLocationhashData.ConvertToLittleEndian();
        }
        public void SetProviderLocationRecord(Dictionary<string, List<string>> hashcollection)
        {
            _dac9aProviderLocationCollection = new ProviderLocationCollection();
            _dac9aProviderLocationCollection.ProviderList = new List<ProviderLocationRecord>();

            ProviderLocationRecord _providerLocRecord = new ProviderLocationRecord();

            foreach (KeyValuePair<string, List<string>> _locRec in hashcollection)
            {
                _providerLocRecord = new ProviderLocationRecord();
                _providerLocRecord.LocationData = _locRec.Key;

                _providerLocRecord.ProviderLocRec = new List<ProviderLocationRecord.LocationRecords>();
                _providerLocRecord.BrandCount = _locRec.Value.Count;
                ProviderLocationRecord.LocationRecords _tempData;
                foreach (string _brand in _locRec.Value)
                {
                    _tempData = new ProviderLocationRecord.LocationRecords();
                    _tempData.BrandIndex = int.Parse(GetBrandIndex(_brand));
                    _tempData.CodesetCount = 0;
                    _tempData.ConvertToByte();
                    _tempData.ConvertToLittleEndian();
                    _providerLocRecord.ProviderLocRec.Add(_tempData);
                }
                _providerLocRecord.ConvertToByte();
                _providerLocRecord.ConvertToLittleEndian();
                _dac9aProviderLocationCollection.ProviderList.Add(_providerLocRecord);

            }
        }

        //Added on 1/21/2014 to handle Top N Manufacturers data
        public void SetTopNManufacturerProviderRecord(List<Locations> _locations, string provider)
        {
            _dac9aProviderLocationhashDataTopNManufacturers = new ProviderLocationHashDataNoZipCodes();
            _dac9aProviderLocationhashDataTopNManufacturers.ProviderName = provider;
            _dac9aProviderLocationhashDataTopNManufacturers.LocationHashCount = _locations.Count;
            _dac9aProviderLocationhashDataTopNManufacturers.LocationHashCollection = new List<ProviderLocationHashDataNoZipCodes.ProviderLocationHash>();
            ProviderLocationHashDataNoZipCodes.ProviderLocationHash _temp = new ProviderLocationHashDataNoZipCodes.ProviderLocationHash();
            for (int i = 0; i < _locations.Count; i++)
            {
                _temp = new ProviderLocationHashDataNoZipCodes.ProviderLocationHash();
                _temp.LocationHashValues = _locations[i].Region + _locations[i].Country;
                _temp.LocationHashOffset = 0;
                _temp.ConvertToByte();
                _temp.ConvertToLittleEndian();
                _dac9aProviderLocationhashDataTopNManufacturers.LocationHashCollection.Add(_temp);
            }
            _dac9aProviderLocationhashDataTopNManufacturers.ConvertToByte();
            _dac9aProviderLocationhashDataTopNManufacturers.ConvertToLittleEndian();

            _dac9aProviderLocationhashDataWithTopNManufacturersCollection.Add(_dac9aProviderLocationhashDataTopNManufacturers);
        }
        public void SetTopNManufacturerProviderLocationRecord(List<Locations> _locations, string provider)
        {
            _dac9aProviderLocationCollectionTopNManufacturers = new ProviderLocationCollectionNoZipCodes();
            _dac9aProviderLocationCollectionTopNManufacturers.ProviderList = new List<ProviderLocationRecordNoZipCodes>();
            _dac9aProviderLocationCollectionTopNManufacturers.ProviderName = provider;
            ProviderLocationRecordNoZipCodes _providerLocRecord = new ProviderLocationRecordNoZipCodes();

            for (int i = 0; i < _locations.Count; i++)
            {
                _providerLocRecord = new ProviderLocationRecordNoZipCodes();
                _providerLocRecord.LocationData = _locations[i].Region + _locations[i].Country;
                _providerLocRecord.ProviderLocRec = new List<ProviderLocationRecordNoZipCodes.LocationRecords>();
                _providerLocRecord.BrandCount = _locations[i].Brandlist.Count;
                ProviderLocationRecordNoZipCodes.LocationRecords _tempData;
                foreach (Brands _brand in _locations[i].Brandlist)
                {
                    _tempData = new ProviderLocationRecordNoZipCodes.LocationRecords();
                    _tempData.BrandIndex = int.Parse(GetBrandIndex(_brand.Name));
                    _tempData.CodesetCount = _brand.Idlist.Count;
                    _tempData.Codesets = _brand.Idlist;
                    _tempData.ConvertToByte();
                    _tempData.ConvertToLittleEndian();
                    _providerLocRecord.ProviderLocRec.Add(_tempData);

                }
                _providerLocRecord.ConvertToByte();
                _providerLocRecord.ConvertToLittleEndian();
                _dac9aProviderLocationCollectionTopNManufacturers.ProviderList.Add(_providerLocRecord);
            }
            _dac9aProviderLocationCollectionTopNManufacturersCollection.Add(_dac9aProviderLocationCollectionTopNManufacturers);
        }


        public void SetTopNManufacturerProviderRecord(Dictionary<string, List<string>> hashcollection)
        {
            _dac9aProviderLocationhashData = new ProviderLocationHashData();
            _dac9aProviderLocationhashData.LocationHashCount = hashcollection.Count;
            _dac9aProviderLocationhashData.LocationHashCollection = new List<ProviderLocationHashData.ProviderLocationHash>();
            ProviderLocationHashData.ProviderLocationHash _temp = new ProviderLocationHashData.ProviderLocationHash();
            foreach (KeyValuePair<string, List<string>> _hash in hashcollection)
            {
                _temp = new ProviderLocationHashData.ProviderLocationHash();
                _temp.LocationHashValues = _hash.Key;
                _temp.LocationHashOffset = 0;
                _temp.ConvertToByte();
                _temp.ConvertToLittleEndian();
                _dac9aProviderLocationhashData.LocationHashCollection.Add(_temp);
            }

            _dac9aProviderLocationhashData.ConvertToByte();
            _dac9aProviderLocationhashData.ConvertToLittleEndian();
        }
        public void SetTopNManufacturerProviderLocationRecord(Dictionary<string, List<string>> hashcollection)
        {
            _dac9aProviderLocationCollection = new ProviderLocationCollection();
            _dac9aProviderLocationCollection.ProviderList = new List<ProviderLocationRecord>();

            ProviderLocationRecord _providerLocRecord = new ProviderLocationRecord();

            foreach (KeyValuePair<string, List<string>> _locRec in hashcollection)
            {
                _providerLocRecord = new ProviderLocationRecord();
                _providerLocRecord.LocationData = _locRec.Key;

                _providerLocRecord.ProviderLocRec = new List<ProviderLocationRecord.LocationRecords>();
                _providerLocRecord.BrandCount = _locRec.Value.Count;
                ProviderLocationRecord.LocationRecords _tempData;
                foreach (string _brand in _locRec.Value)
                {
                    _tempData = new ProviderLocationRecord.LocationRecords();
                    _tempData.BrandIndex = int.Parse(GetBrandIndex(_brand));
                    _tempData.CodesetCount = 0;
                    _tempData.ConvertToByte();
                    _tempData.ConvertToLittleEndian();
                    _providerLocRecord.ProviderLocRec.Add(_tempData);
                }
                _providerLocRecord.ConvertToByte();
                _providerLocRecord.ConvertToLittleEndian();
                _dac9aProviderLocationCollection.ProviderList.Add(_providerLocRecord);

            }
        }
        
       
        public List<byte> GetByteData()
        {
            List<byte> _byteData = new List<byte>();
            List<byte> _tempData = new List<byte>();
            int _addressCounter = 0;
            _tempData=_dac9aFileHeader.GetByteData();
            foreach (byte by in _tempData)
            {
                _byteData.Add(by);
                _addressCounter++;
            }

            _tempData = new List<byte>();
            _tempData=_dac9aHuffmanTree.GetByteData();
            foreach (byte by in _tempData)
            {
                _byteData.Add(by);
                _addressCounter++;
            }

            _tempData = new List<byte>();
            _tempData=_dac9aBrandRecordHeader.GetByteData();
            foreach (byte by in _tempData)
            {
                _byteData.Add(by);
                _addressCounter++;
            }

            _tempData = new List<byte>();
            _tempData = _dac9aBrandRecords.GetByteData();
            foreach (byte by in _tempData)
            {
                _byteData.Add(by);
                _addressCounter++;
            }

            _tempData = new List<byte>();
            _tempData = _dac9aTypeRecordHeader.GetByteData();
            foreach (byte by in _tempData)
            {
                _byteData.Add(by);
                _addressCounter++;
            }

            for (int _typerecs = 0; _typerecs < _dac9aTypeRecordHeader.TypeRecords.Count; _typerecs++)
            {
                _tempData = new List<byte>();
                _tempData = _dac9aTypeRecordHeader.TypeRecords[_typerecs].GetByteData();
                foreach (byte by in _tempData)
                {
                    _byteData.Add(by);
                    _addressCounter++;
                }

                

            }

            for (int _typerecs = 0; _typerecs < _dac9aTypeRecordHeader.TypeRecords.Count; _typerecs++)
            {
                if (_dac9aTypeRecordHeader.TypeRecords[_typerecs].Typename == ".zipcodes")
                {
                    _tempData = new List<byte>();
                    List<byte> _temp = _dac9aTypeLocationhashData.GetByteData();
                    foreach (byte by in _temp)
                    {
                        _tempData.Add(by);
                    }



                    foreach (byte by in _tempData)
                    {
                        _byteData.Add(by);
                    }

                    _tempData = new List<byte>();

                    foreach (LocationRecordCollection _locs in _dac9aLocationRecords)
                    {
                        foreach (LocationRecord _rec in _locs.LocRecords)
                        {

                            List<byte> _tempRecs = _rec.GetByteData();
                            foreach (byte by in _tempRecs)
                            {
                                _tempData.Add(by);
                            }
                        }

                    }

                    foreach (byte by in _tempData)
                    {
                        _byteData.Add(by);
                    }

                }

                else if (_dac9aTypeRecordHeader.TypeRecords[_typerecs].Typename == "Providers")
                {
                    _tempData = new List<byte>();
                    List<byte> _temp = _dac9aProviderLocationhashData.GetByteData();
                    foreach (byte by in _temp)
                    {
                        _tempData.Add(by);
                    }



                    foreach (byte by in _tempData)
                    {
                        _byteData.Add(by);
                    }

                    _tempData = new List<byte>();

                    _tempData = _dac9aProviderLocationCollection.GetByteData();
                    
                    foreach (byte by in _tempData)
                    {
                        _byteData.Add(by);
                    }
                }
                else if (_dac9aTypeRecordHeader.TypeRecords[_typerecs].Typename == "Manufacturers")
                {
                    for (int count = 0; count < _dac9aProviderLocationCollectionTopNManufacturersCollection.Count; count++)
                    {
                        //if (_dac9aTypeRecordHeader.TypeRecords[_typerecs].Typename == _dac9aProviderLocationCollectionTopNManufacturersCollection[count].ProviderName)
                        //{
                            _tempData = new List<byte>();
                            List<byte> _temp = _dac9aProviderLocationhashDataWithTopNManufacturersCollection[count].GetByteData();
                            foreach (byte by in _temp)
                            {
                                _tempData.Add(by);
                            }



                            foreach (byte by in _tempData)
                            {
                                _byteData.Add(by);
                            }

                            _tempData = new List<byte>();

                            _tempData = _dac9aProviderLocationCollectionTopNManufacturersCollection[count].GetByteData();

                            foreach (byte by in _tempData)
                            {
                                _byteData.Add(by);
                            }

                       // }
                    }
                }

                else if (_dac9aTypeRecordHeader.TypeRecords[_typerecs].Typename != ".stb_province" || _dac9aTypeRecordHeader.TypeRecords[_typerecs].Typename != "Manufacturers" || _dac9aTypeRecordHeader.TypeRecords[_typerecs].Typename != "Providers" || _dac9aTypeRecordHeader.TypeRecords[_typerecs].Typename != ".zipcodes")
                {
                    for (int count = 0; count < _dac9aProviderLocationhashDataWithNoZipCodesCollection.Count; count++)
                    {
                        if (_dac9aTypeRecordHeader.TypeRecords[_typerecs].Typename == _dac9aProviderLocationhashDataWithNoZipCodesCollection[count].ProviderName)
                        {
                            _tempData = new List<byte>();
                            List<byte> _temp = _dac9aProviderLocationhashDataWithNoZipCodesCollection[count].GetByteData();
                            foreach (byte by in _temp)
                            {
                                _tempData.Add(by);
                            }



                            foreach (byte by in _tempData)
                            {
                                _byteData.Add(by);
                            }

                            _tempData = new List<byte>();

                            _tempData = _dac9aProviderLocationCollectionNoZipCodesCollection[count].GetByteData();

                            foreach (byte by in _tempData)
                            {
                                _byteData.Add(by);
                            }

                        }
                    }
                }
            }

           

            

            return _byteData;
        }
        public List<byte> GetConvertedByteData()
        {
            List<byte> _byteData = new List<byte>();
            List<byte> _tempData = new List<byte>();
            int _addressCounter = 0;
            _tempData = _dac9aFileHeader.GetConvertedData();
            foreach (byte by in _tempData)
            {
                _byteData.Add(by);
                _addressCounter++;
            }

            _tempData = new List<byte>();
            _tempData = _dac9aHuffmanTree.GetConvertedData();
            foreach (byte by in _tempData)
            {
                _byteData.Add(by);
                _addressCounter++;
            }

            _tempData = new List<byte>();
            _tempData = _dac9aBrandRecordHeader.GetConvertedData();
            foreach (byte by in _tempData)
            {
                _byteData.Add(by);
                _addressCounter++;
            }

            _tempData = new List<byte>();
            _tempData = _dac9aBrandRecords.GetConvertedData();
            foreach (byte by in _tempData)
            {
                _byteData.Add(by);
                _addressCounter++;
            }

            _tempData = new List<byte>();
            _tempData = _dac9aTypeRecordHeader.GetConvertedData();
            foreach (byte by in _tempData)
            {
                _byteData.Add(by);
                _addressCounter++;
            }

            for (int _typerecs = 0; _typerecs < _dac9aTypeRecordHeader.TypeRecords.Count; _typerecs++)
            {
                _tempData = new List<byte>();
                _tempData = _dac9aTypeRecordHeader.TypeRecords[_typerecs].GetConvertedData();
                foreach (byte by in _tempData)
                {
                    _byteData.Add(by);
                    _addressCounter++;
                }



            }

            for (int _typerecs = 0; _typerecs < _dac9aTypeRecordHeader.TypeRecords.Count; _typerecs++)
            {
                if (_dac9aTypeRecordHeader.TypeRecords[_typerecs].Typename == ".zipcodes")
                {
                    _tempData = new List<byte>();
                    List<byte> _temp = _dac9aTypeLocationhashData.GetConvertedData();
                    foreach (byte by in _temp)
                    {
                        _tempData.Add(by);
                    }



                    foreach (byte by in _tempData)
                    {
                        _byteData.Add(by);
                    }

                    _tempData = new List<byte>();

                    foreach (LocationRecordCollection _locs in _dac9aLocationRecords)
                    {
                        foreach (LocationRecord _rec in _locs.LocRecords)
                        {

                            List<byte> _tempRecs = _rec.GetConvertedData();
                            foreach (byte by in _tempRecs)
                            {
                                _tempData.Add(by);
                            }
                        }

                    }

                    foreach (byte by in _tempData)
                    {
                        _byteData.Add(by);
                    }

                    continue;

                }
                else if (_dac9aTypeRecordHeader.TypeRecords[_typerecs].Typename == ".stb_province")
                {
                    //Hash collection starts with the count of all the hashcollections
                    byte[] _reccount = Conversion.ConvertToLittleEndian(Conversion.ConvertToByte(_dac9apctypelochasdatacollection.Count,4), 4);

                    foreach (byte by in _reccount)
                    {
                        _byteData.Add(by);
                    }
                    for (int i = 0; i < _dac9apctypelochasdatacollection.Count; i++)
                    {
                        foreach (byte by in _dac9apctypelochasdatacollection[i].Countrycode)
                        {
                            _byteData.Add(by);
                        }

                        foreach (byte by in _dac9apctypelochasdatacollection[i].Provincecode)
                        {
                            _byteData.Add(by);
                        }

                        foreach (byte by in _dac9apctypelochasdatacollection[i].FileOffset)
                        {
                            _byteData.Add(by);
                        }
                    }

                    for (int i = 0; i < _dac9apctypelocdatacollection.Count; i++)
                    {
                        foreach (byte by in _dac9apctypelocdatacollection[i].Locationdata)
                        {
                            _byteData.Add(by);
                        }
                        foreach (byte by in _dac9apctypelocdatacollection[i].Brandcount)
                        {
                            _byteData.Add(by);
                        }

                        for (int j = 0; j < _dac9apctypelocdatacollection[i].BrandIdCollection.Count; j++)
                        {
                            foreach (byte by in _dac9apctypelocdatacollection[i].BrandIdCollection[j].Brandindex)
                            {
                                _byteData.Add(by);
                            }
                            foreach (byte by in _dac9apctypelocdatacollection[i].BrandIdCollection[j].Cscount)
                            {
                                _byteData.Add(by);
                            }
                            foreach (byte by in _dac9apctypelocdatacollection[i].BrandIdCollection[j].Cslist)
                            {
                                _byteData.Add(by);
                            }

                        }

                    }
                }

                else if (_dac9aTypeRecordHeader.TypeRecords[_typerecs].Typename == "Providers")
                {
                    _tempData = new List<byte>();
                    List<byte> _temp = _dac9aProviderLocationhashData.GetConvertedByteData();
                    foreach (byte by in _temp)
                    {
                        _tempData.Add(by);
                    }



                    foreach (byte by in _tempData)
                    {
                        _byteData.Add(by);
                    }

                    _tempData = new List<byte>();

                    _tempData = _dac9aProviderLocationCollection.GetConvertedData();
                    //foreach (ProviderLocationRecord _locs in _dac9aProviderLocationCollection.ProviderList)
                    //{
                    //    foreach (LocationRecord _rec in _locs.)
                    //    {

                    //        List<byte> _tempRecs = _rec.GetByteData();
                    //        foreach (byte by in _tempRecs)
                    //        {
                    //            _tempData.Add(by);
                    //        }
                    //    }

                    //}

                    foreach (byte by in _tempData)
                    {
                        _byteData.Add(by);
                    }
                }
                else if (_dac9aTypeRecordHeader.TypeRecords[_typerecs].Typename == "Manufacturers")
                {
                    for (int count = 0; count < _dac9aProviderLocationCollectionTopNManufacturersCollection.Count; count++)
                    {
                        //if (_dac9aTypeRecordHeader.TypeRecords[_typerecs].Typename == _dac9aProviderLocationCollectionTopNManufacturersCollection[count].ProviderName)
                        //{
                            _tempData = new List<byte>();
                            List<byte> _temp = _dac9aProviderLocationhashDataWithTopNManufacturersCollection[count].GetConvertedByteData();
                            foreach (byte by in _temp)
                            {
                                _tempData.Add(by);
                            }



                            foreach (byte by in _tempData)
                            {
                                _byteData.Add(by);
                            }

                            _tempData = new List<byte>();

                            _tempData = _dac9aProviderLocationCollectionTopNManufacturersCollection[count].GetConvertedData();

                            foreach (byte by in _tempData)
                            {
                                _byteData.Add(by);
                            }

                       // }
                    }
                }
                else if (_dac9aTypeRecordHeader.TypeRecords[_typerecs].Typename != ".stb_province" || _dac9aTypeRecordHeader.TypeRecords[_typerecs].Typename != "Manufacturers" || _dac9aTypeRecordHeader.TypeRecords[_typerecs].Typename != "Providers" || _dac9aTypeRecordHeader.TypeRecords[_typerecs].Typename != ".zipcodes")
                {
                    for (int count = 0; count < _dac9aProviderLocationhashDataWithNoZipCodesCollection.Count; count++)
                    {
                        if (_dac9aTypeRecordHeader.TypeRecords[_typerecs].Typename == _dac9aProviderLocationhashDataWithNoZipCodesCollection[count].ProviderName)
                        {
                            _tempData = new List<byte>();
                            List<byte> _temp = _dac9aProviderLocationhashDataWithNoZipCodesCollection[count].GetConvertedByteData();
                            foreach (byte by in _temp)
                            {
                                _tempData.Add(by);
                            }



                            foreach (byte by in _tempData)
                            {
                                _byteData.Add(by);
                            }

                            _tempData = new List<byte>();

                            _tempData = _dac9aProviderLocationCollectionNoZipCodesCollection[count].GetConvertedData();

                            foreach (byte by in _tempData)
                            {
                                _byteData.Add(by);
                            }

                        }
                    }
                }
            }





            return _byteData;
        }
        public List<byte> GetConvertedByteData_old()
        {
            List<byte> _byteData = new List<byte>();
            List<byte> _tempData = new List<byte>();
            int _addressCounter = 0;
            _tempData = _dac9aFileHeader.GetConvertedData();
            foreach (byte by in _tempData)
            {
                _byteData.Add(by);
                _addressCounter++;
            }

            _tempData = new List<byte>();
            _tempData = _dac9aHuffmanTree.GetConvertedData();
            foreach (byte by in _tempData)
            {
                _byteData.Add(by);
                _addressCounter++;
            }

            _tempData = new List<byte>();
            _tempData = _dac9aBrandRecordHeader.GetConvertedData();
            foreach (byte by in _tempData)
            {
                _byteData.Add(by);
                _addressCounter++;
            }

            _tempData = new List<byte>();
            _tempData = _dac9aBrandRecords.GetConvertedData();
            foreach (byte by in _tempData)
            {
                _byteData.Add(by);
                _addressCounter++;
            }

            _tempData = new List<byte>();
            _tempData = _dac9aTypeRecordHeader.GetConvertedData();
            foreach (byte by in _tempData)
            {
                _byteData.Add(by);
                _addressCounter++;
            }

            for (int _typerecs = 0; _typerecs < _dac9aTypeRecordHeader.TypeRecords.Count; _typerecs++)
            {
                _tempData = new List<byte>();
                _tempData = _dac9aTypeRecordHeader.TypeRecords[_typerecs].GetConvertedData();
                foreach (byte by in _tempData)
                {
                    _byteData.Add(by);
                    _addressCounter++;
                }
            }



            _tempData = new List<byte>();
            List<byte> _temp = _dac9aTypeLocationhashData.GetConvertedData();
            foreach (byte by in _temp)
            {
                _tempData.Add(by);
            }



            foreach (byte by in _tempData)
            {
                _byteData.Add(by);
            }

            _tempData = new List<byte>();

            foreach (LocationRecordCollection _locs in _dac9aLocationRecords)
            {
                foreach (LocationRecord _rec in _locs.LocRecords)
                {

                    List<byte> _tempRecs = _rec.GetConvertedData();
                    foreach (byte by in _tempRecs)
                    {
                        _tempData.Add(by);
                    }
                }

            }

            foreach (byte by in _tempData)
            {
                _byteData.Add(by);
            }

            return _byteData;
        }
        //public List<byte> GetConvertedByteData()
        //{
        //    List<byte> _byteData = new List<byte>();
        //    List<byte> _tempData = new List<byte>();

        //    _tempData = _dac9aFileHeader.GetConvertedData();
        //    foreach (byte by in _tempData)
        //    {
        //        _byteData.Add(by);
        //    }

        //    _tempData = new List<byte>();
        //    _tempData = _dac9aHuffmanTree.GetConvertedData();
        //    foreach (byte by in _tempData)
        //    {
        //        _byteData.Add(by);
        //    }

        //    _tempData = new List<byte>();
        //    _tempData = _dac9aBrandRecordHeader.GetConvertedData();
        //    foreach (byte by in _tempData)
        //    {
        //        _byteData.Add(by);
        //    }

        //    _tempData = new List<byte>();
        //    _tempData = _dac9aBrandRecords.GetConvertedData();
        //    foreach (byte by in _tempData)
        //    {
        //        _byteData.Add(by);
        //    }

        //    _tempData = new List<byte>();
        //    _tempData = _dac9aTypeRecordHeader.GetConvertedData();
        //    foreach (byte by in _tempData)
        //    {
        //        _byteData.Add(by);
        //    }

           
        //    _tempData = new List<byte>();
        //    List<byte> _temp = _dac9aTypeLocationhashData.GetConvertedData();
        //    foreach (byte by in _temp)
        //    {
        //        _tempData.Add(by);
        //    }

            
        //    foreach (byte by in _tempData)
        //    {
        //        _byteData.Add(by);
        //    }

        //    _tempData = new List<byte>();

        //    foreach (LocationRecordCollection _locs in _dac9aLocationRecords)
        //    {
        //        foreach (LocationRecord _rec in _locs.LocRecords)
        //        {
        //            List<byte> _tempRecs = _rec.GetConvertedData();
        //            foreach (byte by in _tempRecs)
        //            {
        //                _tempData.Add(by);
        //            }
        //        }
        //    }

        //    foreach (byte by in _tempData)
        //    {
        //        _byteData.Add(by);
        //    }

        //    return _byteData;
        //}

        public void CalculateAddressWithNoZipCodes()
        {
            List<byte> _byteData = new List<byte>();
            List<byte> _tempData = new List<byte>();
            Dictionary<int, int> _locationHashValue = new Dictionary<int, int>();
            List<int> _typeRecAddress = new List<int>();
            int _addressCounter = 0;
            _tempData = _dac9aFileHeader.GetByteData();
            foreach (byte by in _tempData)
            {
                _byteData.Add(by);
                _addressCounter++;
            }

            _tempData = new List<byte>();
            _tempData = _dac9aHuffmanTree.GetByteData();
            foreach (byte by in _tempData)
            {
                _byteData.Add(by);
                _addressCounter++;
            }

            _tempData = new List<byte>();
            _tempData = _dac9aBrandRecordHeader.GetByteData();
            foreach (byte by in _tempData)
            {
                _byteData.Add(by);
                _addressCounter++;
            }

            _tempData = new List<byte>();
            _tempData = _dac9aBrandRecords.GetByteData();
            foreach (byte by in _tempData)
            {
                _byteData.Add(by);
                _addressCounter++;
            }

            _tempData = new List<byte>();
            _tempData=_dac9aTypeRecordHeader.GetByteData();
            foreach (byte by in _tempData)
            {
                _byteData.Add(by);
                _addressCounter++;
            }

            for (int _typeRecCount = 0; _typeRecCount < _dac9aTypeRecordHeader.NoTypeRecords; _typeRecCount++)
            {
                _tempData = new List<byte>();
                _tempData = _dac9aTypeRecordHeader.TypeRecords[_typeRecCount].GetByteData();
                foreach (byte by in _tempData)
                {
                    _byteData.Add(by);
                    _addressCounter++;
                }
                
                                
            }

            for (int _typeRecCount = 0; _typeRecCount < _dac9aTypeRecordHeader.NoTypeRecords; _typeRecCount++)
            {
                if (_dac9aTypeRecordHeader.TypeRecords[_typeRecCount].Typename == ".zipcodes")
                {
                    _dac9aTypeRecordHeader.AssignTypeLocationHashDataOffset(_dac9aTypeRecordHeader.TypeRecords[_typeRecCount].Typename, _addressCounter);
                    _tempData = new List<byte>();
                    List<byte> _temp = _dac9aTypeLocationhashData.GetByteData();
                    foreach (byte by in _temp)
                    {
                        _tempData.Add(by);

                    }


                    foreach (byte by in _tempData)
                    {
                        _byteData.Add(by);
                        _addressCounter++;
                    }

                    _tempData = new List<byte>();

                    foreach (LocationRecordCollection _locs in _dac9aLocationRecords)
                    {

                        for (int i = 0; i < _locs.LocRecords.Count; i++)
                        {
                            if (i == 0)
                            {
                                _locationHashValue.Add(_locs.LocRecords[i].LocationData, _addressCounter);
                            }



                            List<byte> _tempRecs = _locs.LocRecords[i].GetByteData();

                            foreach (byte by in _tempRecs)
                            {
                                _tempData.Add(by);
                                _addressCounter++;
                            }
                        }

                    }
                    //_typeRecAddress.Add(_addressCounter);
                    _dac9aTypeLocationhashData.AssignLocationHashData(_locationHashValue);

                    //_dac9aTypeRecordHeader.AssignTypeLocationHashDataOffset(_typeRecAddress);

                    foreach (byte by in _tempData)
                    {
                        _byteData.Add(by);
                    }
                }

                //For Type= Providers

                else
                {
                    _dac9aTypeRecordHeader.AssignTypeLocationHashDataOffset(_dac9aTypeRecordHeader.TypeRecords[_typeRecCount].Typename, _addressCounter);
                    _tempData = new List<byte>();
                    List<byte> _temp = _dac9aProviderLocationhashData.GetByteData();
                    foreach (byte by in _temp)
                    {
                        _tempData.Add(by);

                    }


                    foreach (byte by in _tempData)
                    {
                        _byteData.Add(by);
                        _addressCounter++;
                    }

                    _tempData = new List<byte>();
                    Dictionary<string, int> _providerLocationHashValue = new Dictionary<string, int>();
                    //_tempData = _dac9aProviderLocationCollection.GetByteData();
                    int counter = 0;

                    foreach (ProviderLocationRecord _prlocrec in _dac9aProviderLocationCollection.ProviderList)
                    {
                        _providerLocationHashValue.Add(_prlocrec.LocationData, _addressCounter);
                        List<byte> _tempRecs = _prlocrec.GetByteData();
                        foreach (byte data in _tempRecs)
                        {
                            _tempData.Add(data);
                            _addressCounter++;
                        }

                        //for (int i = 0; i < _prlocrec.ProviderLocRec.Count; i++)
                        //{
                        //    if (counter == 0)
                        //    {
                        //        _providerLocationHashValue.Add(_prlocrec.LocationData, _addressCounter);

                        //    }
                        //    List<byte> _tempRecs = _prlocrec.GetByteData();
                        //    foreach (byte data in _tempRecs)
                        //    {
                        //        _tempData.Add(data);
                        //        _addressCounter++;
                        //    }
                        //    counter++;

                        //}
                        //counter = 0;
                    }



                    //_typeRecAddress.Add(_addressCounter);
                    _dac9aProviderLocationhashData.AssignLocationHashData(_providerLocationHashValue);

                    //_dac9aTypeRecordHeader.AssignTypeLocationHashDataOffset(_typeRecAddress);
                    foreach (byte by in _tempData)
                    {
                        _byteData.Add(by);
                    }
                }
            }

                       
                      

        }
        public void CalculateAddress()
        {
            List<byte> _byteData = new List<byte>();
            List<byte> _tempData = new List<byte>();
            Dictionary<int, int> _locationHashValue = new Dictionary<int, int>();
            List<int> _typeRecAddress = new List<int>();
            int _addressCounter = 0;
            _tempData = _dac9aFileHeader.GetByteData();
            foreach (byte by in _tempData)
            {
                _byteData.Add(by);
                _addressCounter++;
            }

            _tempData = new List<byte>();
            _tempData = _dac9aHuffmanTree.GetByteData();
            foreach (byte by in _tempData)
            {
                _byteData.Add(by);
                _addressCounter++;
            }

            _tempData = new List<byte>();
            _tempData = _dac9aBrandRecordHeader.GetByteData();
            foreach (byte by in _tempData)
            {
                _byteData.Add(by);
                _addressCounter++;
            }

            _tempData = new List<byte>();
            _tempData = _dac9aBrandRecords.GetByteData();
            foreach (byte by in _tempData)
            {
                _byteData.Add(by);
                _addressCounter++;
            }

            _tempData = new List<byte>();
            _tempData=_dac9aTypeRecordHeader.GetByteData();
            foreach (byte by in _tempData)
            {
                _byteData.Add(by);
                _addressCounter++;
            }

            for (int _typeRecCount = 0; _typeRecCount < _dac9aTypeRecordHeader.NoTypeRecords; _typeRecCount++)
            {
                _tempData = new List<byte>();
                _tempData = _dac9aTypeRecordHeader.TypeRecords[_typeRecCount].GetByteData();
                foreach (byte by in _tempData)
                {
                    _byteData.Add(by);
                    _addressCounter++;
                }
                
                                
            }

            for (int _typeRecCount = 0; _typeRecCount < _dac9aTypeRecordHeader.NoTypeRecords; _typeRecCount++)
            {
                
                if (_dac9aTypeRecordHeader.TypeRecords[_typeRecCount].Typename == ".zipcodes")
                {
                    _dac9aTypeRecordHeader.AssignTypeLocationHashDataOffset(_dac9aTypeRecordHeader.TypeRecords[_typeRecCount].Typename, _addressCounter);
                    _tempData = new List<byte>();
                    List<byte> _temp = _dac9aTypeLocationhashData.GetByteData();
                    foreach (byte by in _temp)
                    {
                        _tempData.Add(by);

                    }


                    foreach (byte by in _tempData)
                    {
                        _byteData.Add(by);
                        _addressCounter++;
                    }

                    _tempData = new List<byte>();

                    foreach (LocationRecordCollection _locs in _dac9aLocationRecords)
                    {

                        for (int i = 0; i < _locs.LocRecords.Count; i++)
                        {
                            if (i == 0)
                            {
                                _locationHashValue.Add(_locs.LocRecords[i].LocationData, _addressCounter);
                            }



                            List<byte> _tempRecs = _locs.LocRecords[i].GetByteData();

                            foreach (byte by in _tempRecs)
                            {
                                _tempData.Add(by);
                                _addressCounter++;
                            }
                        }

                    }
                    //_typeRecAddress.Add(_addressCounter);
                    _dac9aTypeLocationhashData.AssignLocationHashData(_locationHashValue);

                    //_dac9aTypeRecordHeader.AssignTypeLocationHashDataOffset(_typeRecAddress);

                    foreach (byte by in _tempData)
                    {
                        _byteData.Add(by);
                    }
                    continue;
                }

                if (_dac9aTypeRecordHeader.TypeRecords[_typeRecCount].Typename == ".stb_province")
                {
                    _dac9aTypeRecordHeader.AssignTypeLocationHashDataOffset(_dac9aTypeRecordHeader.TypeRecords[_typeRecCount].Typename, _addressCounter);
                    _addressCounter += 4;//This is to store the count of the number of hash collection
                    for (int i = 0; i < _dac9apctypelochasdatacollection.Count; i++)
                    {
                        _addressCounter += 8;
                    }
                    Dictionary<long, int> _offsetaddress = new Dictionary<long, int>();
                    bool _locationhaschanged = false;
                    string _loc=string.Empty;
                    string _location = string.Empty; ;
                    string _prevprv = string.Empty; ;
                    for (int i = 0; i < _dac9apctypelocdatacollection.Count; i++)
                    {
                        long _locs=Conversion.ConvertByteArrayToInt32(_dac9apctypelocdatacollection[i].Locationdata);
                        if (i == 0)
                        {
                             _location = _locs.ToString();
                             _offsetaddress.Add(_locs, _addressCounter);
                             _prevprv = GetProvince(_locs.ToString("X"));


                        }
                        string _newlocation = _locs.ToString();
                        string _newprv = GetProvince(_locs.ToString("X"));
                        if (_newprv != _prevprv)
                        {
                            _locationhaschanged = true;
                        }
                        if (_locationhaschanged)
                        {
                            _offsetaddress.Add(_locs, _addressCounter);
                            _location = Conversion.ConvertByteArrayToInt32(_dac9apctypelocdatacollection[i].Locationdata).ToString();
                            _prevprv = GetProvince(_locs.ToString("X"));
                            _locationhaschanged = false;
                        }
                        _addressCounter += 5;
                        for (int j = 0; j < _dac9apctypelocdatacollection[i].BrandIdCollection.Count; j++)
                        {
                            _addressCounter += 2 + 1 + (Conversion.ConvertBytetoInt(_dac9apctypelocdatacollection[i].BrandIdCollection[j].Cscount[0])*3);
                        }
                    }
                    //_offsetaddress.Add(Int32.Parse(_location), _addressCounter);
                    SetPCLocationTypeHashOffsetValues(_offsetaddress);
                }

                //For Type= Providers

                if (_dac9aTypeRecordHeader.TypeRecords[_typeRecCount].Typename == "Providers" )
                {
                    string _prtype = _dac9aTypeRecordHeader.TypeRecords[_typeRecCount].Typename;

                    _dac9aTypeRecordHeader.AssignTypeLocationHashDataOffset(_dac9aTypeRecordHeader.TypeRecords[_typeRecCount].Typename, _addressCounter);
                    
                    //If Top N Provider

                    
                        _tempData = new List<byte>();
                        List<byte> _temp = _dac9aProviderLocationhashData.GetByteData();
                        foreach (byte by in _temp)
                        {
                            _tempData.Add(by);

                        }


                        foreach (byte by in _tempData)
                        {
                            _byteData.Add(by);
                            _addressCounter++;
                        }

                        _tempData = new List<byte>();
                        Dictionary<string, int> _providerLocationHashValue = new Dictionary<string, int>();
                        //_tempData = _dac9aProviderLocationCollection.GetByteData();
                        int counter = 0;

                        foreach (ProviderLocationRecord _prlocrec in _dac9aProviderLocationCollection.ProviderList)
                        {
                            _providerLocationHashValue.Add(_prlocrec.LocationData, _addressCounter);
                            List<byte> _tempRecs = _prlocrec.GetByteData();
                            foreach (byte data in _tempRecs)
                            {
                                _tempData.Add(data);
                                _addressCounter++;
                            }


                        }



                        //_typeRecAddress.Add(_addressCounter);
                        _dac9aProviderLocationhashData.AssignLocationHashData(_providerLocationHashValue);

                        //_dac9aTypeRecordHeader.AssignTypeLocationHashDataOffset(_typeRecAddress);
                        foreach (byte by in _tempData)
                        {
                            _byteData.Add(by);
                        }
                    

                    

                }
                else if (_dac9aTypeRecordHeader.TypeRecords[_typeRecCount].Typename == "Manufacturers")
                {
                    
                    for (int count = 0; count < _dac9aProviderLocationCollectionTopNManufacturersCollection.Count; count++)
                    {
                       // if (_dac9aTypeRecordHeader.TypeRecords[_typeRecCount].Typename == _dac9aProviderLocationCollectionTopNManufacturersCollection[count].ProviderName)
                        //{
                            _dac9aTypeRecordHeader.AssignTypeLocationHashDataOffset(_dac9aTypeRecordHeader.TypeRecords[_typeRecCount].Typename, _addressCounter);
                            _tempData = new List<byte>();
                            List<byte> _temp = _dac9aProviderLocationhashDataWithTopNManufacturersCollection[count].GetByteData();
                            //List<byte> _temp = _dac9aProviderLocationCollectionTopNManufacturersCollection[count].GetByteData();
                            foreach (byte by in _temp)
                            {
                                _tempData.Add(by);

                            }


                            foreach (byte by in _tempData)
                            {
                                _byteData.Add(by);
                                _addressCounter++;
                            }

                            _tempData = new List<byte>();

                            Dictionary<string, int> _providerLocationHashValue = new Dictionary<string, int>();
                            //_tempData = _dac9aProviderLocationCollection.GetByteData();
                            int counter = 0;

                            foreach (ProviderLocationRecordNoZipCodes _prlocrec in _dac9aProviderLocationCollectionTopNManufacturersCollection[count].ProviderList)
                            {
                                _providerLocationHashValue.Add(_prlocrec.LocationData, _addressCounter);
                                List<byte> _tempRecs = _prlocrec.GetByteData();
                                foreach (byte data in _tempRecs)
                                {
                                    _tempData.Add(data);
                                    _addressCounter++;
                                }


                            }

                            //_typeRecAddress.Add(_addressCounter);
                            
                            _dac9aProviderLocationhashDataWithTopNManufacturersCollection[count].AssignLocationHashData(_providerLocationHashValue);
                            //_dac9aTypeRecordHeader.AssignTypeLocationHashDataOffset(_typeRecAddress);
                            foreach (byte by in _tempData)
                            {
                                _byteData.Add(by);
                            }


                        //}
                    }
                }
                else if (_dac9aTypeRecordHeader.TypeRecords[_typeRecCount].Typename != ".stb_province" && _dac9aTypeRecordHeader.TypeRecords[_typeRecCount].Typename != "Manufacturers" && _dac9aTypeRecordHeader.TypeRecords[_typeRecCount].Typename != "Providers" && _dac9aTypeRecordHeader.TypeRecords[_typeRecCount].Typename != ".zipcodes")
                {
                    int loopcounter = 0;
                    for (int count = 0; count < _dac9aProviderLocationhashDataWithNoZipCodesCollection.Count; count++)
                    {
                        if (_dac9aTypeRecordHeader.TypeRecords[_typeRecCount].Typename == _dac9aProviderLocationhashDataWithNoZipCodesCollection[count].ProviderName)
                        {
                            _dac9aTypeRecordHeader.AssignTypeLocationHashDataOffset(_dac9aTypeRecordHeader.TypeRecords[_typeRecCount].Typename, _addressCounter);
                            _tempData = new List<byte>();
                            List<byte> _temp = _dac9aProviderLocationhashDataWithNoZipCodesCollection[count].GetByteData();
                            foreach (byte by in _temp)
                            {
                                _tempData.Add(by);

                            }


                            foreach (byte by in _tempData)
                            {
                                _byteData.Add(by);
                                _addressCounter++;
                            }

                            _tempData = new List<byte>();

                            Dictionary<string, int> _providerLocationHashValue = new Dictionary<string, int>();
                            //_tempData = _dac9aProviderLocationCollection.GetByteData();
                            int counter = 0;

                            foreach (ProviderLocationRecordNoZipCodes _prlocrec in _dac9aProviderLocationCollectionNoZipCodesCollection[count].ProviderList)
                            {
                                try
                                {
                                    _providerLocationHashValue.Add(_prlocrec.LocationData, _addressCounter);
                                    List<byte> _tempRecs = _prlocrec.GetByteData();
                                    foreach (byte data in _tempRecs)
                                    {
                                        _tempData.Add(data);
                                        _addressCounter++;
                                    }
                                }
                                catch (Exception ex)
                                {
                                    throw new Exception(ex.Message);
                                }


                            }

                            //_typeRecAddress.Add(_addressCounter);
                            _dac9aProviderLocationhashDataWithNoZipCodesCollection[count].AssignLocationHashData(_providerLocationHashValue);

                            //_dac9aTypeRecordHeader.AssignTypeLocationHashDataOffset(_typeRecAddress);
                            foreach (byte by in _tempData)
                            {
                                _byteData.Add(by);
                            }


                        }

                        loopcounter++;
                        if (loopcounter < _dac9aProviderLocationhashDataWithNoZipCodesCollection.Count)
                        {
                            _typeRecCount++;
                        }
                    }
                   
                    
                }
            }

                       
                      

        }

        private void SetPCLocationTypeHashOffsetValues(Dictionary<long,int> addrs)
        {
            List<int> _vals = addrs.Values.ToList();
            if (_vals.Count == _dac9apctypelochasdatacollection.Count)
            {

                for (int i = 0; i < _dac9apctypelochasdatacollection.Count; i++)
                {
                    _dac9apctypelochasdatacollection[i].FileOffset = Conversion.ConvertToLittleEndian(Conversion.ConvertToByte(_vals[i], 4),4);
                }
            }
            else
            {
                throw new Exception("Something is wrong..");
            }
        }

        private string GetBrandIndex(string brand)
        {
            string _status = null;
            int _index = 0;
            foreach (BrandRecord _br in _dac9aBrandRecords.Brands)
            {
                if (brand == _br.BrandName)
                {
                    _status = _index.ToString();
                    break;
                }
                else
                    _index++;

            }
            return _status;
        }

        private string _libVersion;

        public string LibVersion
        {
            get { return _libVersion; }
            set { _libVersion = value; }
        }
        public void GetAssemblyInfo()
        {
            //this.Text = String.Format("About {0}", AssemblyTitle);
            //this.labelProductName.Text = AssemblyProduct;
            LibVersion = String.Format("Version {0}", AssemblyVersion);
            //this.labelCopyright.Text = AssemblyCopyright;
            //this.labelCompanyName.Text = AssemblyCompany;
            //this.textBoxDescription.Text = AssemblyDescription;
        }

        public string AssemblyVersion
        {
            get
            {
                return Assembly.GetExecutingAssembly().GetName().Version.ToString();
            }
        }
       
        public DAC9aData()
        {
            _dac9aFileHeader = new FileHeader();
            _dac9aHuffmanTree = new HuffmanTree();
            _dac9aBrandRecordHeader = new BrandRecordHeader();
            _dac9aBrandRecords = new BrandCollections();
            
            _dac9aTypeRecordHeader = new TypeRecordHeader();
            _dac9aTypeRecord = new TypeRecordHeader.TypeRecord();
            _dac9aTypeLocationhashData = new TypeLocationHashData();
            _dac9aLocationRecords = new List<LocationRecordCollection>(PageSize);
            GetAssemblyInfo();
        }
        
        public class FileHeader
        {
            
            string _fileType;
            public string FileType
            {
                get { return _fileType; }
                set { _fileType = value; }
            }
            int _fileVersion;
            public int FileVersion
            {
                get { return _fileVersion; }
                set { _fileVersion = value; }
            }
            int _fileFlags;
            public int FileFlags
            {
                get { return _fileFlags; }
                set { _fileFlags = value; }
            }

            byte[] _byteFileType;
            public byte[] ByteFileType
            {
                get { return _byteFileType; }
                set { _byteFileType = value; }
            }
            byte[] _convertedbyteFileType;
            public byte[] ConvertedbyteFileType
            {
                get { return _convertedbyteFileType; }
                set { _convertedbyteFileType = value; }
            }
            
            byte[] _byteFileVersion;
            public byte[] ByteFileVersion
            {
                get { return _byteFileVersion; }
                set { _byteFileVersion = value; }
            }
            byte[] _convertedbyteFileVersion;
            public byte[] ConvertedbyteFileVersion
            {
                get { return _convertedbyteFileVersion; }
                set { _convertedbyteFileVersion = value; }
            }

            byte[] _byteFileFlags;

            public byte[] ByteFileFlags
            {
                get { return _byteFileFlags; }
                set { _byteFileFlags = value; }
            }
            byte[] _convertedByteFileFlags;

            public byte[] ConvertedByteFileFlags
            {
                get { return _convertedByteFileFlags; }
                set { _convertedByteFileFlags = value; }
            }

            public FileHeader()
            {
                _fileType = string.Empty;
                _fileFlags = 0;
                _fileVersion = 0;

                ByteFileType = new byte[4];
                ConvertedbyteFileType = new byte[4];
                ByteFileVersion = new byte[4];
                ConvertedbyteFileVersion = new byte[4];
                ByteFileFlags = new byte[4];
                ConvertedByteFileFlags = new byte[4];
                           
            }

            public void ConvertToByte()
            {
               
                ByteFileType = Conversion.ConvertToByte(FileType,ByteFileType.Length);
                ByteFileVersion = Conversion.ConvertToByte(FileVersion, ByteFileVersion.Length);
                ByteFileFlags = Conversion.ConvertToByte(FileFlags, ByteFileFlags.Length);
                
                    
            }
                        
            public void ConvertToEndian(ByteType _type)
            {
                switch (_type)
                {
                    case ByteType.LittleEndian:
                        {
                            //ConvertedbyteFileType = Conversion.ConvertToLittleEndian(ByteFileType,ByteFileType.Length);
                            ConvertedbyteFileType = ByteFileType;
                            ConvertedbyteFileVersion = Conversion.ConvertToLittleEndian(ByteFileVersion, ByteFileVersion.Length);
                            ConvertedByteFileFlags = Conversion.ConvertToLittleEndian(ByteFileFlags, ByteFileFlags.Length);
                            break;
                        }
                    case ByteType.BigEndian:
                        {

                            break;
                        }
                }
            }

            public int GetEndAddress(int startaddress)
            {
                int endAddress = startaddress + ByteFileType.Length + ByteFileVersion.Length + ByteFileFlags.Length;
                return endAddress;
            }

            public List<byte> GetByteData()
            {
                List<byte> _buffer = new List<byte>();
                //for (int i = 0; i < ByteFileType.Length; i++)
                //{
                //    _buffer.Add(ByteFileType[i]);
                //}

                //for (int i = 0; i < ByteFileVersion.Length; i++)
                //{
                //    _buffer.Add(ByteFileVersion[i]);
                //}

                //for (int i = 0; i < ByteFileFlags.Length; i++)
                //{
                //    _buffer.Add(ByteFileFlags[i]);
                //}

                foreach (byte _by in ByteFileType)
                {
                    _buffer.Add(_by);
                }
                foreach (byte _by in ByteFileVersion)
                {
                    _buffer.Add(_by);
                }

                foreach (byte _by in ByteFileFlags)
                {
                    _buffer.Add(_by);
                }


                return _buffer;
            }

            public List<byte> GetConvertedData()
            {
                List<byte> _buffer = new List<byte>();
                foreach (byte _by in ConvertedbyteFileType)
                {
                    _buffer.Add(_by);
                }
                foreach (byte _by in ConvertedbyteFileVersion)
                {
                    _buffer.Add(_by);
                }

                foreach (byte _by in ConvertedByteFileFlags)
                {
                    _buffer.Add(_by);
                }


                return _buffer;
            }

        }
        public class HuffmanTree
        {
            
            int _treeLen;

            public int TreeLen
            {
                get { return _treeLen; }
                set { _treeLen = value; }
            }


            private List<byte> _treeData;

            public List<byte> TreeData
            {
                get { return _treeData; }
                set { _treeData = value; }
            }

            byte[] _byteTreeData;

            public byte[] ByteTreeData
            {
                get { return _byteTreeData; }
                set { _byteTreeData = value; }
            }

            byte[] _byteTreLen;

            public byte[] ByteTreLen
            {
                get { return _byteTreLen; }
                set { _byteTreLen = value; }
            }
            byte[] _convertedByteTreeLen;

            public byte[] ConvertedByteTreeLen
            {
                get { return _convertedByteTreeLen; }
                set { _convertedByteTreeLen = value; }
            }

            byte[] _convertedByteTreeData;

            public byte[] ConvertedByteTreeData
            {
                get { return _convertedByteTreeData; }
                set { _convertedByteTreeData = value; }
            }

            public HuffmanTree()
            {
                _treeLen = 0;
                _treeData = new List<byte>(); //Currently not used in this version of DAC9a
                _byteTreLen = new byte[2];
                _convertedByteTreeLen = new byte[2];
            }

            public void ConvertToByte()
            {

                ByteTreLen = Conversion.ConvertToByte(TreeLen, ByteTreLen.Length);
                //ByteTreeData = Conversion.ConvertToByte();
                
            }
            public int GetEndAddress(int startaddress)
            {
                int endAddress = startaddress + ByteTreLen.Length;// +ByteTreeData.Length;
                
                return endAddress;
            }
            public void ConvertToEndian(ByteType _type)
            {
                switch (_type)
                {
                    case ByteType.LittleEndian:
                        {
                            ConvertedByteTreeLen = Conversion.ConvertToLittleEndian(ByteTreLen,ByteTreLen.Length);
                            //ConvertedByteTreeData = Conversion.ConvertToLittleEndian(ByteTreeData);
                            break;
                        }
                    case ByteType.BigEndian:
                        {

                            break;
                        }
                }
            }

            public List<byte> GetByteData()
            {
                List<byte> _byteData = new List<byte>();
                foreach (byte by in ByteTreLen)
                {
                    _byteData.Add(by);
                }
                //foreach (byte by in ByteTreeData)
                //{
                //    _byteData.Add(by);
                //}

                return _byteData;
            }

            public List<byte> GetConvertedData()
            {
                List<byte> _byteData = new List<byte>();
                foreach (byte by in ConvertedByteTreeLen)
                {
                    _byteData.Add(by);
                }
                //foreach (byte by in ConvertedByteTreeData)
                //{
                //    _byteData.Add(by);
                //}
                return _byteData;
            }
        }
        public class BrandRecordHeader
        {
            string _brandHeader;
            
            public string BrandHeader
            {
                get { return _brandHeader; }
                set { _brandHeader = value; }
            }
            byte[] _byteBrandHeader;

            public byte[] ByteBrandHeader
            {
                get { return _byteBrandHeader; }
                set { _byteBrandHeader = value; }
            }
            byte[] _convertedByteBrandHeader;

            public byte[] ConvertedByteBrandHeader
            {
                get { return _convertedByteBrandHeader; }
                set { _convertedByteBrandHeader = value; }
            }
            int _brandRecordSize;

            public int BrandRecordSize
            {
                get { return _brandRecordSize; }
                set { _brandRecordSize = value; }
            }
            byte[] _byteBrandRecordSize;

            public byte[] ByteBrandRecordSize
            {
                get { return _byteBrandRecordSize; }
                set { _byteBrandRecordSize = value; }
            }
            byte[] _convertedBrandRecordSize;

            public byte[] ConvertedBrandRecordSize
            {
                get { return _convertedBrandRecordSize; }
                set { _convertedBrandRecordSize = value; }
            }
            int _brandsCount;

            public int BrandsCount
            {
                get { return _brandsCount; }
                set { _brandsCount = value; }
            }
            byte[] _byteBrandsCount;

            public byte[] ByteBrandsCount
            {
                get { return _byteBrandsCount; }
                set { _byteBrandsCount = value; }
            }
            byte[] _convertedBrandsCount;

            public byte[] ConvertedBrandsCount
            {
                get { return _convertedBrandsCount; }
                set { _convertedBrandsCount = value; }
            }

            public BrandRecordHeader()
            {
                _brandHeader = string.Empty;
                _brandRecordSize = 0;
                _brandsCount = 0;
                _byteBrandHeader = new byte[2];
                _byteBrandRecordSize = new byte[4];
                _byteBrandsCount = new byte[2];
                _convertedByteBrandHeader = new byte[2];
                _convertedBrandRecordSize = new byte[4];
                _convertedBrandsCount = new byte[2];
            }

            public void ConvertToByte()
            {

                ByteBrandHeader = Conversion.ConvertToByte(BrandHeader, ByteBrandHeader.Length);
                ByteBrandRecordSize = Conversion.ConvertToByte(BrandRecordSize, ByteBrandRecordSize.Length);
                ByteBrandsCount = Conversion.ConvertToByte(BrandsCount, ByteBrandsCount.Length);

            }

            public int GetEndAddress(int startaddress)
            {
                int endAddress = startaddress + ByteBrandHeader.Length + ByteBrandRecordSize.Length + ByteBrandsCount.Length;
                
                return endAddress;
            }

            public void ConvertToEndian(ByteType _type)
            {
                switch (_type)
                {
                    case ByteType.LittleEndian:
                        {
                            //ConvertedByteBrandHeader = Conversion.ConvertToLittleEndian(ByteBrandHeader,ByteBrandHeader.Length);
                            ConvertedByteBrandHeader = ByteBrandHeader;
                            ConvertedBrandRecordSize = Conversion.ConvertToLittleEndian(ByteBrandRecordSize, ByteBrandRecordSize.Length);
                            ConvertedBrandsCount = Conversion.ConvertToLittleEndian(ByteBrandsCount, ByteBrandsCount.Length);

                            break;
                        }
                    case ByteType.BigEndian:
                        {

                            break;
                        }
                }
            }

            public List<byte> GetByteData()
            {
                List<byte> _byteData = new List<byte>();
                foreach (byte by in ByteBrandHeader)
                {
                    _byteData.Add(by);
                }
                foreach (byte by in ByteBrandRecordSize)
                {
                    _byteData.Add(by);
                }
                foreach (byte by in ByteBrandsCount)
                {
                    _byteData.Add(by);
                }
                return _byteData;
            }

            public List<byte> GetConvertedData()
            {
                List<byte> _byteData = new List<byte>();
                foreach (byte by in ConvertedByteBrandHeader)
                {
                    _byteData.Add(by);
                }
                foreach (byte by in ConvertedBrandRecordSize)
                {
                    _byteData.Add(by);
                }
                foreach (byte by in ConvertedBrandsCount)
                {
                    _byteData.Add(by);
                }
                return _byteData;
            }
        }
        public class BrandCollections
        {
            List<BrandRecord> _brands;
            
            public List<BrandRecord> Brands
            {
                get { return _brands; }
                set { _brands = value; }
            }

            public BrandCollections()
            {
                _brands = new List<BrandRecord>();
            }

            public List<byte> GetByteData()
            {
                List<byte> _byteData = new List<byte>();
                foreach (BrandRecord br in _brands)
                {
                    List<byte> b = br.GetByteData();
                    foreach (byte data in b)
                    {
                        _byteData.Add(data);
                    }
                }
                return _byteData;
            }

            public List<byte> GetConvertedData()
            {
                List<byte> _byteData = new List<byte>();
                foreach (BrandRecord br in _brands)
                {
                    List<byte> b = br.GetConvertedData();
                    foreach (byte data in b)
                    {
                        _byteData.Add(data);
                    }
                }
                return _byteData;
            }
        }
        public class BrandRecord
        {
            
            int _recordSize;

            public int RecordSize
            {
                get { return _recordSize; }
                set { _recordSize = value; }
            }
            byte[] _byteRecordSize;

            public byte[] ByteRecordSize
            {
                get { return _byteRecordSize; }
                set { _byteRecordSize = value; }
            }
            byte[] _convertedRecordSize;

            public byte[] ConvertedRecordSize
            {
                get { return _convertedRecordSize; }
                set { _convertedRecordSize = value; }
            }

            string _brandName;

            public string BrandName
            {
                get { return _brandName; }
                set { _brandName = value; }
            }
            byte[] _byteBrandName;

            public byte[] ByteBrandName
            {
                get { return _byteBrandName; }
                set { _byteBrandName = value; }
            }
            byte[] _convertedBrandName;

            public byte[] ConvertedBrandName
            {
                get { return _convertedBrandName; }
                set { _convertedBrandName = value; }
            }

            public BrandRecord()
            {
                _recordSize = 0;
                _byteRecordSize = new byte[1];
                _convertedRecordSize = new byte[1];

                _brandName = string.Empty;
                //_byteBrandName = new byte[_brandName.Length];
                //_convertedBrandName = new byte[_brandName.Length];
            }

            public void ConvertToByte()
            {

                
                ByteRecordSize = Conversion.ConvertToByte(RecordSize, ByteRecordSize.Length);
                //ByteBrandName = Conversion.ConvertToByte(BrandName, BrandName.Length);//Need to convert UTF 8 to ASCII first, need to revisit this code
                ByteBrandName = Conversion.ConvertUTF16ToUTF8(BrandName);
                //ByteBrandName = Conversion.ConvertISO3166toByte(BrandName);
                               
               
            }

            public int GetEndAddress(int startaddress)
            {
                int endAddress = startaddress + ByteRecordSize.Length + BrandName.Length;
                
                return endAddress;
            }

            public void ConvertToEndian(ByteType _type)
            {
                switch (_type)
                {
                    case ByteType.LittleEndian:
                        {
                            //ConvertedBrandName = Conversion.ConvertToLittleEndian(ByteBrandName,ByteBrandName.Length);
                            ConvertedBrandName = ByteBrandName;
                            ConvertedRecordSize = Conversion.ConvertToLittleEndian(ByteRecordSize, ByteRecordSize.Length);
                            
                            break;
                        }
                    case ByteType.BigEndian:
                        {

                            break;
                        }
                }
            }

            public List<byte> GetByteData()
            {
                List<byte> _byteData = new List<byte>();
                foreach (byte by in ByteRecordSize)
                {
                    _byteData.Add(by);
                }
                foreach (byte by in ByteBrandName)
                {
                    _byteData.Add(by);
                }
                return _byteData;
            }

            public List<byte> GetConvertedData()
            {
                List<byte> _byteData = new List<byte>();
                foreach (byte by in ConvertedRecordSize)
                {
                    _byteData.Add(by);
                }
                foreach (byte by in ConvertedBrandName)
                {
                    _byteData.Add(by);
                }
                return _byteData;
            }


        }
        public class TypeRecordHeader
        {
            
            string _typeRecHeader;
            public string TypeRecHeader
            {
                get { return _typeRecHeader; }
                set { _typeRecHeader = value; }
            }

            int _noTypeRecords;
            public int NoTypeRecords
            {
                get { return _noTypeRecords; }
                set { _noTypeRecords = value; }
            }

            public class TypeRecord
            {
                int _typeNameLen;
                
                public int TypeNameLen
                {
                    get { return _typeNameLen; }
                    set { _typeNameLen = value; }
                }
                string _typename;//This is in UTF8 and needs to be converted to ASCII

                public string Typename
                {
                    get { return _typename; }
                    set { _typename = value; }
                }
                int _osmNamelen;

                public int OsmNamelen
                {
                    get { return _osmNamelen; }
                    set { _osmNamelen = value; }
                }
                string _osmName;

                public string OsmName
                {
                    get { return _osmName; }
                    set { _osmName = value; }
                }
                int _typeLocHashDataFileOffset;

                public int TypeLocHashDataFileOffset
                {
                    get { return _typeLocHashDataFileOffset; }
                    set { _typeLocHashDataFileOffset = value; }
                }

                byte[] _byteTypeNameLen;

                public byte[] ByteTypeNameLen
                {
                    get { return _byteTypeNameLen; }
                    set { _byteTypeNameLen = value; }
                }
                byte[] _byteTypeName;

                public byte[] ByteTypeName
                {
                    get { return _byteTypeName; }
                    set { _byteTypeName = value; }
                }
                byte[] _byteOSMNameLen;

                public byte[] ByteOSMNameLen
                {
                    get { return _byteOSMNameLen; }
                    set { _byteOSMNameLen = value; }
                }
                byte[] _byteOSMName;

                public byte[] ByteOSMName
                {
                    get { return _byteOSMName; }
                    set { _byteOSMName = value; }
                }
                byte[] _byteTypeLochashData;

                public byte[] ByteTypeLochashData
                {
                    get { return _byteTypeLochashData; }
                    set { _byteTypeLochashData = value; }
                }

                byte[] _convertedTypeNameLen;

                public byte[] ConvertedTypeNameLen
                {
                    get { return _convertedTypeNameLen; }
                    set { _convertedTypeNameLen = value; }
                }
                byte[] _convertedTypeName;

                public byte[] ConvertedTypeName
                {
                    get { return _convertedTypeName; }
                    set { _convertedTypeName = value; }
                }
                byte[] _convertedOSMNameLen;

                public byte[] ConvertedOSMNameLen
                {
                    get { return _convertedOSMNameLen; }
                    set { _convertedOSMNameLen = value; }
                }
                byte[] _convertedOSMName;

                public byte[] ConvertedOSMName
                {
                    get { return _convertedOSMName; }
                    set { _convertedOSMName = value; }
                }
                byte[] _convertedTypeLochashdata;

                public byte[] ConvertedTypeLochashdata
                {
                    get { return _convertedTypeLochashdata; }
                    set { _convertedTypeLochashdata = value; }
                }

                public TypeRecord()
                {
                    _typeNameLen = 0;
                    _typename = string.Empty;
                    _osmName = string.Empty;
                    _osmNamelen = 0;
                    _typeLocHashDataFileOffset = 0;

                    _byteOSMName = new byte[3];
                    _byteOSMNameLen = new byte[1];
                    _byteTypeLochashData = new byte[4];
                    _byteTypeName = new byte[4];
                    //_byteTypeName = new byte[9];
                    _byteTypeNameLen = new byte[1];

                    _convertedOSMName = new byte[3];
                    _convertedOSMNameLen = new byte[1];
                    _convertedTypeLochashdata = new byte[4];
                    _convertedTypeName = new byte[4];
                    //_convertedTypeName = new byte[9];
                    _convertedTypeNameLen = new byte[1];
                }

                public void ConvertToByte()
                {

                    ByteOSMName = Conversion.ConvertUTF16ToUTF8(OsmName);
                    ByteOSMNameLen = Conversion.ConvertToByte(OsmNamelen, ByteOSMNameLen.Length);
                    ByteTypeLochashData = Conversion.ConvertToByte(TypeLocHashDataFileOffset, ByteTypeLochashData.Length);
                    //ByteTypeName = Conversion.ConvertToByte(Typename, ByteTypeName.Length);
                    ByteTypeName = Conversion.ConvertUTF16ToUTF8(Typename);
                    ByteTypeNameLen = Conversion.ConvertToByte(TypeNameLen, ByteTypeNameLen.Length);


                }

                public int GetEndAddress(int startaddress)
                {
                    int endAddress = startaddress + ByteOSMName.Length + ByteOSMNameLen.Length + ByteTypeLochashData.Length + ByteTypeName.Length + ByteTypeNameLen.Length;
                    
                    return endAddress;
                }

                public void ConvertToEndian(ByteType _type)
                {
                    switch (_type)
                    {
                        case ByteType.LittleEndian:
                            {
                                //ConvertedOSMName = Conversion.ConvertToLittleEndian(ByteOSMName, ByteOSMName.Length);
                                ConvertedOSMName = ByteOSMName;
                                ConvertedOSMNameLen = Conversion.ConvertToLittleEndian(ByteOSMNameLen, ByteOSMNameLen.Length);//This is one byte
                                ConvertedTypeLochashdata = Conversion.ConvertToLittleEndian(ByteTypeLochashData, ByteTypeLochashData.Length);
                                //ConvertedTypeName = Conversion.ConvertToLittleEndian(ByteTypeName, ByteTypeName.Length);
                                ConvertedTypeName = ByteTypeName;
                                ConvertedTypeNameLen = Conversion.ConvertToLittleEndian(ByteTypeNameLen, ByteTypeNameLen.Length);//This is one byte

                                break;
                            }
                        case ByteType.BigEndian:
                            {

                                break;
                            }
                    }
                }
                public List<byte> GetByteData()
                {
                    List<byte> _byteData = new List<byte>();
                    foreach (byte by in ByteTypeNameLen)
                    {
                        _byteData.Add(by);
                    }
                    foreach (byte by in ByteTypeName)
                    {
                        _byteData.Add(by);
                    }
                    foreach (byte by in ByteOSMNameLen)
                    {
                        _byteData.Add(by);
                    }
                    foreach (byte by in ByteOSMName)
                    {
                        _byteData.Add(by);
                    }
                    foreach (byte by in ByteTypeLochashData)
                    {
                        _byteData.Add(by);
                    }
                    return _byteData;
                }

                public List<byte> GetConvertedData()
                {
                    List<byte> _byteData = new List<byte>();
                    foreach (byte by in ConvertedTypeNameLen)
                    {
                        _byteData.Add(by);
                    }
                    foreach (byte by in ConvertedTypeName)
                    {
                        _byteData.Add(by);
                    }
                    foreach (byte by in ConvertedOSMNameLen)
                    {
                        _byteData.Add(by);
                    }
                    foreach (byte by in ConvertedOSMName)
                    {
                        _byteData.Add(by);
                    }
                    foreach (byte by in ConvertedTypeLochashdata)
                    {
                        _byteData.Add(by);
                    }
                    return _byteData;
                }
            }

            List<TypeRecord> _typeRecords;

            public void AssignTypeLocationHashDataOffset(string typename, int address)
            {
                for (int typeRecCount = 0; typeRecCount < TypeRecords.Count; typeRecCount++)
                {
                    if (TypeRecords[typeRecCount].Typename == typename)
                    {
                        TypeRecords[typeRecCount].TypeLocHashDataFileOffset = address;
                        TypeRecords[typeRecCount].ConvertToByte();
                        TypeRecords[typeRecCount].ConvertToEndian(ByteType.LittleEndian);
                        TypeRecords[typeRecCount].GetByteData();
                        TypeRecords[typeRecCount].GetConvertedData();
                    }
                }
            }

            public void AssignTypeLocationHashDataOffset(List<int> Address)
            {
                for(int i=0;i<Address.Count;i++)
                {
                    TypeRecords[i].TypeLocHashDataFileOffset = Address[i];
                    TypeRecords[i].ConvertToByte();
                    TypeRecords[i].ConvertToEndian(ByteType.LittleEndian);
                    TypeRecords[i].GetByteData();
                    TypeRecords[i].GetConvertedData();
                }

            }

            public List<TypeRecord> TypeRecords
            {
                get { return _typeRecords; }
                set { _typeRecords = value; }
            }
            
            //int _typeRecord;
            //public int TypeRecord
            //{
            //    get { return _typeRecord; }
            //    set { _typeRecord = value; }
            //}
            
            byte[] _byteTypeRecordHeader;
            public byte[] ByteTypeRecordHeader
            {
                get { return _byteTypeRecordHeader; }
                set { _byteTypeRecordHeader = value; }
            }
            
            byte[] _byteNoTypeRecords;
            public byte[] ByteNoTypeRecords
            {
                get { return _byteNoTypeRecords; }
                set { _byteNoTypeRecords = value; }
            }
            
            //byte[] _byteTypeRecord;
            //public byte[] ByteTypeRecord
            //{
            //    get { return _byteTypeRecord; }
            //    set { _byteTypeRecord = value; }
            //}

            byte[] _convertedTypeRecordHeader;

            public byte[] ConvertedTypeRecordHeader
            {
                get { return _convertedTypeRecordHeader; }
                set { _convertedTypeRecordHeader = value; }
            }
            byte[] _convertedNoTypeRecords;

            public byte[] ConvertedNoTypeRecords
            {
                get { return _convertedNoTypeRecords; }
                set { _convertedNoTypeRecords = value; }
            }
            //byte[] _convertedTypeRecord;

            //public byte[] ConvertedTypeRecord
            //{
            //    get { return _convertedTypeRecord; }
            //    set { _convertedTypeRecord = value; }
            //}

            public void ConvertToByte()
            {

                ByteTypeRecordHeader = Conversion.ConvertToByte(TypeRecHeader, ByteTypeRecordHeader.Length);
                ByteNoTypeRecords = Conversion.ConvertToByte(NoTypeRecords, ByteNoTypeRecords.Length);
                
                //ByteTypeRecord = Conversion.ConvertToByte(TypeRecord, ByteTypeRecord.Length);
               

            }

            public int GetEndAddress(int startaddress)
            {
                int endAddress = startaddress + ByteTypeRecordHeader.Length + ByteNoTypeRecords.Length;// +ByteTypeRecord.Length;
                
                return endAddress;
            }

            public void ConvertToEndian(ByteType _type)
            {
                switch (_type)
                {
                    case ByteType.LittleEndian:
                        {
                            //ConvertedTypeRecordHeader = Conversion.ConvertToLittleEndian(ByteTypeRecordHeader, ByteTypeRecordHeader.Length);
                            ConvertedTypeRecordHeader = ByteTypeRecordHeader;
                            ConvertedNoTypeRecords = Conversion.ConvertToLittleEndian(ByteNoTypeRecords, ByteNoTypeRecords.Length);
                            //ConvertedTypeRecord = Conversion.ConvertToLittleEndian(ByteTypeRecord, ByteTypeRecord.Length);
                           
                            break;
                        }
                    case ByteType.BigEndian:
                        {

                            break;
                        }
                }
            }

            public List<byte> GetByteData()
            {
                List<byte> _byteData = new List<byte>();
                foreach (byte by in ByteTypeRecordHeader)
                {
                    _byteData.Add(by);
                }
                foreach (byte by in ByteNoTypeRecords)
                {
                    _byteData.Add(by);
                }

                //foreach (TypeRecordHeader.TypeRecord _tyrec in TypeRecords)
                //{
                //    List<byte> _data = _tyrec.GetByteData();
                //    foreach (byte by in _data)
                //    {
                //        _byteData.Add(by);
                //    }
                //}
                
                return _byteData;
            }

            public List<byte> GetConvertedData()
            {
                List<byte> _byteData = new List<byte>();
                foreach (byte by in ConvertedTypeRecordHeader)
                {
                    _byteData.Add(by);
                }
                foreach (byte by in ConvertedNoTypeRecords)
                {
                    _byteData.Add(by);
                }
                //foreach (byte by in ConvertedTypeRecord)
                //{
                //    _byteData.Add(by);
                //}

                //foreach (TypeRecordHeader.TypeRecord _tyrec in TypeRecords)
                //{
                //    List<byte> _data = _tyrec.GetConvertedData();
                //    foreach (byte by in _data)
                //    {
                //        _byteData.Add(by);
                //    }
                //}


                return _byteData;
            }

            

            public TypeRecordHeader()
            {
                _typeRecHeader = string.Empty;
                _noTypeRecords = 0;
                TypeRecords = new List<TypeRecord>();
               // _typeRecord = 0;

                _byteTypeRecordHeader = new byte[2];
                _byteNoTypeRecords = new byte[4];
                //_byteTypeRecord = new byte[1];

                _convertedTypeRecordHeader = new byte[2];
                _convertedNoTypeRecords = new byte[4];
                //_convertedTypeRecord = new byte[1];

            }

        }

        #region For Zip Codes
        public class TypeLocationHashData
        {
           
            int _locationHashCount;
            public int LocationHashCount
            {
                get { return _locationHashCount; }
                set { _locationHashCount = value; }
            }

            List<HashValues> _locationCollection = new List<HashValues>();
            public List<HashValues> LocationCollection
            {
                get { return _locationCollection; }
                set { _locationCollection = value; }
            }

            public void AssignLocationHashData(string typename, Dictionary<int, int> _locationhashData)
            {

            }

            public void AssignLocationHashData(Dictionary<int, int> _locationhashData)
            {
                int counter=0;
                foreach (KeyValuePair<int, int> _hashdata in _locationhashData)
                {
                    _locationCollection[counter].LocationHashValue = _hashdata.Key;
                    _locationCollection[counter].LocationFileOffset = _hashdata.Value;
                    _locationCollection[counter].ConvertToByte();
                    _locationCollection[counter].ConvertToEndian(ByteType.LittleEndian);
                    _locationCollection[counter].GetByteData();
                    _locationCollection[counter].GetConvertedData();
                    counter++;
                }
            }

            public class HashValues
            {

                public HashValues()
                {
                    _locationHashValue = 0;
                    _locationFileOffset = 0;

                    _bytelocationHashValue = new byte[4];
                    _bytelocationFileOffset = new byte[4];
                    _convertedlocationHashValue = new byte[4];
                    _convertedlocationFileOffset = new byte[4];
                }
                int _locationHashValue;
                public int LocationHashValue
                {
                    get { return _locationHashValue; }
                    set { _locationHashValue = value; }

                }
                int _locationFileOffset;
                public int LocationFileOffset
                {
                    get { return _locationFileOffset; }
                    set { _locationFileOffset = value; }
                }

                byte[] _bytelocationHashValue;

                public byte[] BytelocationHashValue
                {
                    get { return _bytelocationHashValue; }
                    set { _bytelocationHashValue = value; }
                }
                byte[] _bytelocationFileOffset;

                public byte[] BytelocationFileOffset
                {
                    get { return _bytelocationFileOffset; }
                    set { _bytelocationFileOffset = value; }
                }

                byte[] _convertedlocationHashValue;

                public byte[] ConvertedlocationHashValue
                {
                    get { return _convertedlocationHashValue; }
                    set { _convertedlocationHashValue = value; }
                }
                byte[] _convertedlocationFileOffset;

                public byte[] ConvertedlocationFileOffset
                {
                    get { return _convertedlocationFileOffset; }
                    set { _convertedlocationFileOffset = value; }
                }

                public void ConvertToByte()
                {


                    _bytelocationHashValue = Conversion.ConvertToByte(_locationHashValue, _bytelocationHashValue.Length);
                    _bytelocationFileOffset = Conversion.ConvertToByte(_locationFileOffset, _bytelocationFileOffset.Length);


                }

                public int GetEndAddress(int startaddress)
                {
                    int endAddress = startaddress + _bytelocationHashValue.Length + _bytelocationFileOffset.Length;
                    return endAddress;
                }

                public void ConvertToEndian(ByteType _type)
                {
                    switch (_type)
                    {
                        case ByteType.LittleEndian:
                            {

                                _convertedlocationHashValue = Conversion.ConvertToLittleEndian(_bytelocationHashValue, _bytelocationHashValue.Length);
                                _convertedlocationFileOffset = Conversion.ConvertToLittleEndian(_bytelocationFileOffset, _bytelocationFileOffset.Length);

                                break;
                            }
                        case ByteType.BigEndian:
                            {

                                break;
                            }
                    }
                }
                public List<byte> GetByteData()
                {
                    List<byte> _byteData = new List<byte>();

                    foreach (byte by in BytelocationHashValue)
                    {
                        _byteData.Add(by);
                    }
                    foreach (byte by in BytelocationFileOffset)
                    {
                        _byteData.Add(by);
                    }
                    return _byteData;
                }

                public List<byte> GetConvertedData()
                {
                    List<byte> _byteData = new List<byte>();

                    foreach (byte by in ConvertedlocationHashValue)
                    {
                        _byteData.Add(by);
                    }
                    foreach (byte by in ConvertedlocationFileOffset)
                    {
                        _byteData.Add(by);
                    }

                    return _byteData;
                }



            }

            byte[] _bytelocationhashCount;

            public byte[] BytelocationhashCount
            {
                get { return _bytelocationhashCount; }
                set { _bytelocationhashCount = value; }
            }


            byte[] _convertedlocationhashCount;

            public byte[] ConvertedlocationhashCount
            {
                get { return _convertedlocationhashCount; }
                set { _convertedlocationhashCount = value; }
            }
            public TypeLocationHashData()
            {
                _locationHashCount = 0;


                _bytelocationhashCount = new byte[4];


                _convertedlocationhashCount = new byte[4];


            }

            public void ConvertToByte()
            {

                _bytelocationhashCount = Conversion.ConvertToByte(_locationHashCount, _bytelocationhashCount.Length);
                //_bytelocationHashValue = Conversion.ConvertToByte(_locationHashValue, _bytelocationHashValue.Length);
                //_bytelocationFileOffset = Conversion.ConvertToByte(_locationFileOffset, _bytelocationFileOffset.Length);


            }

            public int GetEndAddress(int startaddress)
            {
                int endAddress = startaddress + _bytelocationhashCount.Length;
                return endAddress;
            }

            public void ConvertToEndian(ByteType _type)
            {
                switch (_type)
                {
                    case ByteType.LittleEndian:
                        {
                            _convertedlocationhashCount = Conversion.ConvertToLittleEndian(_bytelocationhashCount, _bytelocationhashCount.Length);
                            //_convertedlocationHashValue = Conversion.ConvertToLittleEndian(_bytelocationHashValue, _bytelocationHashValue.Length);
                            //_convertedlocationFileOffset = Conversion.ConvertToLittleEndian(_bytelocationFileOffset, _bytelocationFileOffset.Length);

                            break;
                        }
                    case ByteType.BigEndian:
                        {

                            break;
                        }
                }
            }
            public List<byte> GetByteData()
            {
                List<byte> _byteData = new List<byte>();
                foreach (byte by in BytelocationhashCount)
                {
                    _byteData.Add(by);
                }

                foreach (TypeLocationHashData.HashValues _lst in LocationCollection)
                {
                    foreach (byte _hashVal in _lst.BytelocationHashValue)
                    {
                        _byteData.Add(_hashVal);
                    }
                    foreach (byte _hashOffset in _lst.BytelocationFileOffset)
                    {
                        _byteData.Add(_hashOffset);
                    }
                }
                //foreach (byte by in BytelocationHashValue)
                //{
                //    _byteData.Add(by);
                //}
                //foreach (byte by in BytelocationFileOffset)
                //{
                //    _byteData.Add(by);
                //}
                return _byteData;
            }

            public List<byte> GetConvertedData()
            {
                List<byte> _byteData = new List<byte>();
                foreach (byte by in ConvertedlocationhashCount)
                {
                    _byteData.Add(by);
                }
                //foreach (byte by in ConvertedlocationHashValue)
                //{
                //    _byteData.Add(by);
                //}
                //foreach (byte by in ConvertedlocationFileOffset)
                //{
                //    _byteData.Add(by);
                //}

                foreach (TypeLocationHashData.HashValues _loc in LocationCollection)
                {
                    List<byte> _tempData = _loc.GetConvertedData();
                    foreach (byte by in _tempData)
                    {
                        _byteData.Add(by);
                    }
                    
                }

                return _byteData;
            }



        }
        public class LocationRecord
        {
            
            int _locationData;

            public int LocationData
            {
                get { return _locationData; }
                set { _locationData = value; }
            }
            int _brandCount;

            public int BrandCount
            {
                get { return _brandCount; }
                set { _brandCount = value; }
            }

            List<BrandIds> _brandIdCollection;

            public List<BrandIds> BrandIdCollection
            {
                get { return _brandIdCollection; }
                set { _brandIdCollection = value; }
            }

            public class BrandIds
            {
                int _brandIndex;

                public int BrandIndex
                {
                    get { return _brandIndex; }
                    set { _brandIndex = value; }
                }
                int _codesetCount;

                public int CodesetCount
                {
                    get { return _codesetCount; }
                    set { _codesetCount = value; }
                }
                List<string> _codesets;

                public List<string> Codesets
                {
                    get { return _codesets; }
                    set { _codesets = value; }
                }
                byte[] _byteBrandIndex;

                public byte[] ByteBrandIndex
                {
                    get { return _byteBrandIndex; }
                    set { _byteBrandIndex = value; }
                }

                byte[] _byteCodesetCount;

                public byte[] ByteCodesetCount
                {
                    get { return _byteCodesetCount; }
                    set { _byteCodesetCount = value; }
                }

                List<byte[]> _byteCodesets;

                public List<byte[]> ByteCodesets
                {
                    get { return _byteCodesets; }
                    set { _byteCodesets = value; }
                }
                byte[] _convertedBrandIndex;

                byte[] _convertedCodesetCount;

                public byte[] ConvertedCodesetCount
                {
                    get { return _convertedCodesetCount; }
                    set { _convertedCodesetCount = value; }
                }

                public byte[] ConvertedBrandIndex
                {
                    get { return _convertedBrandIndex; }
                    set { _convertedBrandIndex = value; }
                }
                List<byte[]> _convertedCodesets;

                public List<byte[]> ConvertedCodesets
                {
                    get { return _convertedCodesets; }
                    set { _convertedCodesets = value; }
                }

                public BrandIds()
                {
                    _brandIndex = 0;
                    _codesetCount = 0;
                    _codesets = new List<string>();


                    ByteBrandIndex = new byte[2];
                    ByteCodesetCount = new byte[1];
                    ByteCodesets = new List<byte[]>();//Each Id to have 3 bytes each


                    ConvertedBrandIndex = new byte[2];
                    ConvertedCodesetCount = new byte[1];
                    ConvertedCodesets = new List<byte[]>();//Each Id to have 3 bytes each
                }

                public void ConvertToByte()
                {
                    ByteBrandIndex = Conversion.ConvertToByte(BrandIndex, ByteBrandIndex.Length);
                    ByteCodesetCount = Conversion.ConvertToByte(CodesetCount, ByteCodesetCount.Length);
                    byte[] _byteCs = new byte[3];
                    foreach (string cs in Codesets)
                    {
                        _byteCs = new byte[3];
                        string _mode = cs.Substring(0, 1);
                        byte[] _byteMode = Conversion.ConvertToByte(_mode, _mode.Length);
                        string _setupcode = cs.Substring(1, 4);
                        byte[] _byteSetupCode = Conversion.ConvertToByte(int.Parse(_setupcode), 2);
                        byte[] _convertedByte = new byte[3];
                        _convertedByte[0] = _byteMode[0];
                        _convertedByte[1] = _byteSetupCode[0];
                        _convertedByte[2] = _byteSetupCode[1];
                        //_byteCs = Conversion.ConvertToByte(cs, _byteCs.Length);//T2051 T uses 1 byte and 2051 uses other 2 bytes
                        ByteCodesets.Add(_convertedByte);

                    }
                }

                public void ConvertToEndian(ByteType _type)
                {
                    switch (_type)
                    {
                        case ByteType.LittleEndian:
                            {
                                
                                ConvertedBrandIndex = Conversion.ConvertToLittleEndian(ByteBrandIndex, ByteBrandIndex.Length);
                                ConvertedCodesetCount = Conversion.ConvertToLittleEndian(ByteCodesetCount, ByteCodesetCount.Length);
                                byte[] _byteCs = new byte[3];
                                foreach (byte[] cs in ByteCodesets)
                                {
                                    _byteCs = new byte[3];
                                    _byteCs = Conversion.ConvertToLittleEndian(cs, 3);
                                    ConvertedCodesets.Add(_byteCs);

                                }

                                break;
                            }
                        case ByteType.BigEndian:
                            {

                                break;
                            }
                    }
                }

                public List<byte> GetByteData()
                {
                    List<byte> _byteData = new List<byte>();
                    
                    foreach (byte by in ByteBrandIndex)
                    {
                        _byteData.Add(by);
                    }
                    foreach (byte by in ByteCodesetCount)
                    {
                        _byteData.Add(by);
                    }
                    foreach (byte[] by in ByteCodesets)
                    {
                        foreach (byte b in by)
                        {
                            _byteData.Add(b);
                        }
                    }

                    return _byteData;
                }

                public List<byte> GetConvertedData()
                {
                    List<byte> _byteData = new List<byte>();
                    
                    foreach (byte by in ConvertedBrandIndex)
                    {
                        _byteData.Add(by);
                    }
                    foreach (byte by in ConvertedCodesetCount)
                    {
                        _byteData.Add(by);
                    }
                    foreach (byte[] by in ConvertedCodesets)
                    {
                        foreach (byte b in by)
                        {
                            _byteData.Add(b);
                        }
                    }

                    return _byteData;
                }

            }

            byte[] _byteLocationData;

            public byte[] ByteLocationData
            {
                get { return _byteLocationData; }
                set { _byteLocationData = value; }
            }
            byte[] _byteBrandCount;

            public byte[] ByteBrandCount
            {
                get { return _byteBrandCount; }
                set { _byteBrandCount = value; }
            }
            
            byte[] _convertedLocationData;

            public byte[] ConvertedLocationData
            {
                get { return _convertedLocationData; }
                set { _convertedLocationData = value; }
            }
            byte[] _convertedBrandCount;

            public byte[] ConvertedBrandCount
            {
                get { return _convertedBrandCount; }
                set { _convertedBrandCount = value; }
            }
            

            public LocationRecord()
            {
                _locationData = 0;
                _brandCount = 0;

                ByteLocationData = new byte[4];
                ByteBrandCount = new byte[1];

                ConvertedLocationData = new byte[4];
                ConvertedBrandCount = new byte[1];

                _brandIdCollection = new List<BrandIds>();

            }

            public int GetEndAddress(int startaddress)
            {
                int endAddress = startaddress + ByteLocationData.Length + ByteBrandCount.Length;// +ByteBrandIndex.Length + ByteCodesetCount.Length;
                return endAddress;
            }

            public void ConvertToByte()
            {

                ByteLocationData = Conversion.ConvertToByte(LocationData, ByteLocationData.Length);
                ByteBrandCount = Conversion.ConvertToByte(BrandCount, ByteBrandCount.Length);
                




            }

            public void ConvertToEndian(ByteType _type)
            {
                switch (_type)
                {
                    case ByteType.LittleEndian:
                        {
                            ConvertedLocationData = Conversion.ConvertToLittleEndian(ByteLocationData, ByteLocationData.Length);
                            ConvertedBrandCount = Conversion.ConvertToLittleEndian(ByteBrandCount, ByteBrandCount.Length);
                            //ConvertedBrandIndex = Conversion.ConvertToLittleEndian(ByteBrandIndex, ByteBrandIndex.Length);
                            //ConvertedCodesetCount = Conversion.ConvertToLittleEndian(ByteCodesetCount, ByteCodesetCount.Length);
                            //byte[] _byteCs = new byte[3];
                            //foreach (byte[] cs in ByteCodesets)
                            //{
                            //    _byteCs = new byte[3];
                            //    _byteCs = Conversion.ConvertToLittleEndian(cs, 3);
                            //    ConvertedCodesets.Add(_byteCs);

                            //}

                            break;
                        }
                    case ByteType.BigEndian:
                        {

                            break;
                        }
                }
            }

            public List<byte> GetByteData()
            {
                List<byte> _byteData = new List<byte>();
                foreach (byte by in ByteLocationData)
                {
                    _byteData.Add(by);
                }
                foreach (byte by in ByteBrandCount)
                {
                    _byteData.Add(by);
                }

                foreach (BrandIds _bid in _brandIdCollection)
                {
                    foreach (byte by in _bid.ByteBrandIndex)
                    {
                        _byteData.Add(by);
                    }
                    foreach (byte by in _bid.ByteCodesetCount)
                    {
                        _byteData.Add(by);
                    }
                    foreach (byte[] by in _bid.ByteCodesets)
                    {
                        foreach (byte b in by)
                        {
                            _byteData.Add(b);
                        }
                    }

                }
                //foreach (byte by in ByteBrandIndex)
                //{
                //    _byteData.Add(by);
                //}
                //foreach (byte by in ByteCodesetCount)
                //{
                //    _byteData.Add(by);
                //}
                //foreach (byte[] by in ByteCodesets)
                //{
                //    foreach (byte b in by)
                //    {
                //        _byteData.Add(b);
                //    }
                //}

                return _byteData;
            }

            public List<byte> GetConvertedData()
            {
                List<byte> _byteData = new List<byte>();
                foreach (byte by in ConvertedLocationData)
                {
                    _byteData.Add(by);
                }
                foreach (byte by in ConvertedBrandCount)
                {
                    _byteData.Add(by);
                }

                foreach (BrandIds _bid in BrandIdCollection)
                {
                    foreach (byte by in _bid.ConvertedBrandIndex)
                    {
                        _byteData.Add(by);
                    }
                    foreach (byte by in _bid.ConvertedCodesetCount)
                    {
                        _byteData.Add(by);
                    }
                    foreach (byte[] by in _bid.ConvertedCodesets)
                    {
                        foreach (byte b in by)
                        {
                            _byteData.Add(b);
                        }
                    }

                }
                //foreach (byte by in ByteBrandIndex)
                //{
                //    _byteData.Add(by);
                //}
                //foreach (byte by in ByteCodesetCount)
                //{
                //    _byteData.Add(by);
                //}
                //foreach (byte[] by in ByteCodesets)
                //{
                //    foreach (byte b in by)
                //    {
                //        _byteData.Add(b);
                //    }
                //}

                return _byteData;
            }

            //public List<byte> GetConvertedData()
            //{
            //    List<byte> _byteData = new List<byte>();
            //    foreach (byte by in ConvertedLocationData)
            //    {
            //        _byteData.Add(by);
            //    }
            //    foreach (byte by in ConvertedBrandCount)
            //    {
            //        _byteData.Add(by);
            //    }

            //    foreach (BrandIds _bid in _brandIdCollection)
            //    {
            //        foreach (byte by in _bid.ConvertedBrandIndex)
            //        {
            //            _byteData.Add(by);
            //        }
            //        foreach (byte by in _bid.ConvertedCodesetCount)
            //        {
            //            _byteData.Add(by);
            //        }
            //        foreach (byte[] by in _bid.ConvertedCodesets)
            //        {
            //            foreach (byte b in by)
            //            {
            //                _byteData.Add(b);
            //            }
            //        }

            //    }
            //    //foreach (byte by in ConvertedBrandIndex)
            //    //{
            //    //    _byteData.Add(by);
            //    //}
            //    //foreach (byte by in ConvertedCodesetCount)
            //    //{
            //    //    _byteData.Add(by);
            //    //}
            //    //foreach (byte[] by in ConvertedCodesets)
            //    //{
            //    //    foreach (byte b in by)
            //    //    {
            //    //        _byteData.Add(b);
            //    //    }
            //    //}

            //    return _byteData;
            //}


        }
        public class LocationRecordCollection
        {
            
            List<LocationRecord> _locRecords;
            public List<LocationRecord> LocRecords
            {
                get { return _locRecords; }
                set { _locRecords = value; }
            }

            public LocationRecordCollection()
            {
                
                _locRecords=new List<LocationRecord>();
            }
         }
        #endregion

        #region For Top N Providers

        public class ProviderLocationHashData
        {

            public ProviderLocationHashData()
            {
                _locationHashCount = 0;
                _locationHashCollection = new List<ProviderLocationHash>();

                _byteLocationHashCount = new byte[4];
                _convertedLocationHashCount = new byte[4];
            }
            int _locationHashCount;

            public int LocationHashCount
            {
                get { return _locationHashCount; }
                set { _locationHashCount = value; }
            }
            byte[] _byteLocationHashCount;

            public byte[] ByteLocationHashCount
            {
                get { return _byteLocationHashCount; }
                set { _byteLocationHashCount = value; }
            }
            byte[] _convertedLocationHashCount;

            public byte[] ConvertedLocationHashCount
            {
                get { return _convertedLocationHashCount; }
                set { _convertedLocationHashCount = value; }
            }

            List<ProviderLocationHash> _locationHashCollection;

            public List<ProviderLocationHash> LocationHashCollection
            {
                get { return _locationHashCollection; }
                set { _locationHashCollection = value; }
            }

            public void ConvertToByte()
            {
                ByteLocationHashCount = Conversion.ConvertToByte(_locationHashCount,ByteLocationHashCount.Length);
            }

            public void ConvertToLittleEndian()
            {
                ConvertedLocationHashCount = Conversion.ConvertTo4ByteLittleEndian(_locationHashCount);
            }

            public List<byte> GetByteData()
            {
                List<byte> byteData = new List<byte>();
                foreach (byte data in ByteLocationHashCount)
                {
                    byteData.Add(data);
                }

                foreach (ProviderLocationHash _lochash in LocationHashCollection)
                {
                    List<byte> _temp = new List<byte>();

                    _temp = _lochash.GetByteData();
                    foreach (byte data in _temp)
                    {
                        byteData.Add(data);
                    }
                }


                return byteData;
            }

            public List<byte> GetConvertedByteData()
            {
                List<byte> byteData = new List<byte>();
                foreach (byte data in ConvertedLocationHashCount)
                {
                    byteData.Add(data);
                }

                foreach (ProviderLocationHash _lochash in LocationHashCollection)
                {
                    List<byte> _temp = new List<byte>();

                    _temp = _lochash.GetConvertedByteData();
                    foreach (byte data in _temp)
                    {
                        byteData.Add(data);
                    }
                }


                return byteData;
            }

            public void AssignLocationHashData(Dictionary<string, int> _locationhashData)
            {
                int counter = 0;
                foreach (KeyValuePair<string, int> _hashdata in _locationhashData)
                {

                    LocationHashCollection[counter].LocationHashValues = _hashdata.Key;
                    LocationHashCollection[counter].LocationHashOffset = _hashdata.Value;
                    LocationHashCollection[counter].ConvertToByte();
                    LocationHashCollection[counter].ConvertToLittleEndian();
                    LocationHashCollection[counter].GetByteData();
                    LocationHashCollection[counter].GetConvertedByteData();
                    counter++;
                }
            }

            public class ProviderLocationHash
            {
                public ProviderLocationHash()
                {
                    _locationHashValues = string.Empty;
                    _locationHashOffset = 0;
                    _byteLocationHashOffset = new byte[4];
                    _byteLocationHashValue = new byte[4];
                    _convertedLocationHashOffset = new byte[4];
                    _convertedLocationHashValue = new byte[4];
                }

                string _locationHashValues;

                public string LocationHashValues
                {
                    get { return _locationHashValues; }
                    set { _locationHashValues = value; }
                }
                byte[] _byteLocationHashValue;

                public byte[] ByteLocationHashValue
                {
                    get { return _byteLocationHashValue; }
                    set { _byteLocationHashValue = value; }
                }
                byte[] _convertedLocationHashValue;

                public byte[] ConvertedLocationHashValue
                {
                    get { return _convertedLocationHashValue; }
                    set { _convertedLocationHashValue = value; }
                }

                int _locationHashOffset;

                public int LocationHashOffset
                {
                    get { return _locationHashOffset; }
                    set { _locationHashOffset = value; }
                }
                byte[] _byteLocationHashOffset;

                public byte[] ByteLocationHashOffset
                {
                    get { return _byteLocationHashOffset; }
                    set { _byteLocationHashOffset = value; }
                }
                byte[] _convertedLocationHashOffset;

                public byte[] ConvertedLocationHashOffset
                {
                    get { return _convertedLocationHashOffset; }
                    set { _convertedLocationHashOffset = value; }
                }

                public void ConvertToByte()
                {
                    ByteLocationHashValue = Conversion.ConvertUTF16ToUTF8(LocationHashValues);
                    ByteLocationHashOffset = Conversion.ConvertToByte(LocationHashOffset, ByteLocationHashOffset.Length);
                }

                public void ConvertToLittleEndian()
                {
                    ConvertedLocationHashValue = Conversion.ConvertUTF16ToUTF8(LocationHashValues);
                    ConvertedLocationHashOffset = Conversion.ConvertTo4ByteLittleEndian(LocationHashOffset);
                }

                public List<byte> GetByteData()
                {
                    List<byte> byteData = new List<byte>();

                    foreach (byte data in ByteLocationHashValue)
                    {
                        byteData.Add(data);
                    }

                    foreach (byte data in ByteLocationHashOffset)
                    {
                        byteData.Add(data);
                    }

                    return byteData;
                }

                public List<byte> GetConvertedByteData()
                {
                    List<byte> byteData = new List<byte>();

                    foreach (byte data in ConvertedLocationHashValue)
                    {
                        byteData.Add(data);
                    }

                    foreach (byte data in ConvertedLocationHashOffset)
                    {
                        byteData.Add(data);
                    }

                    return byteData;
                }
            }

        }
        public class ProviderLocationRecord
        {
            public ProviderLocationRecord()
            {
                LocationData = string.Empty;
                ByteLocationData = new byte[4];
                ConvertedLocationData = new byte[4];

                _brandCount = 0;
                _byteBrandCount = new byte();
                _convertedBrandCount = new byte();
            }

            public void ConvertToByte()
            {
                //Assuming that the bit 7 of byte 0 is 0 for now, needs to be changed later

                ByteLocationData = Conversion.ConvertUTF16ToUTF8(LocationData);
                ByteBrandCount = (byte)BrandCount;
            }
            public void ConvertToLittleEndian()
            {
                ConvertedLocationData = Conversion.ConvertUTF16ToUTF8(LocationData);
                ConvertedBrandCount = (byte)BrandCount;
            }

            byte _byte0Data = 0x00;

            string _locationData;
            public string LocationData
            {
                get { return _locationData; }
                set { _locationData = value; }
            }
            byte[] _byteLocationData;
            public byte[] ByteLocationData
            {
                get { return _byteLocationData; }
                set { _byteLocationData = value; }
            }

            byte[] _convertedLocationData;
            public byte[] ConvertedLocationData
            {
                get { return _convertedLocationData; }
                set { _convertedLocationData = value; }
            }

            List<LocationRecords> _providerLocRec;
            public List<LocationRecords> ProviderLocRec
            {
                get { return _providerLocRec; }
                set { _providerLocRec = value; }
            }

            int _brandCount = 0;
            public int BrandCount
            {
                get { return _brandCount; }
                set { _brandCount = value; }
            }
            byte _byteBrandCount;
            public byte ByteBrandCount
            {
                get { return _byteBrandCount; }
                set { _byteBrandCount = value; }
            }
            byte _convertedBrandCount = 0;
            public byte ConvertedBrandCount
            {
                get { return _convertedBrandCount; }
                set { _convertedBrandCount = value; }
            }

            

            public List<byte> GetByteData()
            {
                List<byte> byteData = new List<byte>();

                foreach (byte data in ByteLocationData)
                {
                    byteData.Add(data);
                }

                byteData.Add(ByteBrandCount);


                List<byte> _temp = new List<byte>();

                foreach (LocationRecords _locrec in ProviderLocRec)
                {
                    _temp = new List<byte>();
                    _temp = _locrec.GetByteData();
                    foreach (byte data in _temp)
                    {
                        byteData.Add(data);
                    }
                }

                return byteData;
            }

            public List<byte> GetConvertedByteData()
            {
                List<byte> byteData = new List<byte>();

                foreach (byte data in ConvertedLocationData)
                {
                    byteData.Add(data);
                }

                byteData.Add(ConvertedBrandCount);

                List<byte> _temp = new List<byte>();

                foreach (LocationRecords _locrec in ProviderLocRec)
                {
                    _temp = new List<byte>();
                    _temp = _locrec.GetConvertedByteData();
                    foreach (byte data in _temp)
                    {
                        byteData.Add(data);
                    }
                }

                return byteData;
            }

            public class LocationRecords
            {
                public LocationRecords()
                {

                    _brandIndex = 0;
                    _byteBrandIndex = new byte[2];
                    _convertedBrandIndex = new byte[2];

                    _codesetCount = 0;
                    _byteCodesetCount = new byte();
                    _convertedCodesetCount = new byte();
                }


                int _brandIndex = 0;

                public int BrandIndex
                {
                    get { return _brandIndex; }
                    set { _brandIndex = value; }
                }
                byte[] _byteBrandIndex;

                public byte[] ByteBrandIndex
                {
                    get { return _byteBrandIndex; }
                    set { _byteBrandIndex = value; }
                }
                byte[] _convertedBrandIndex;

                public byte[] ConvertedBrandIndex
                {
                    get { return _convertedBrandIndex; }
                    set { _convertedBrandIndex = value; }
                }

                int _codesetCount = 0;

                public int CodesetCount
                {
                    get { return _codesetCount; }
                    set { _codesetCount = value; }
                }
                byte _byteCodesetCount;

                public byte ByteCodesetCount
                {
                    get { return _byteCodesetCount; }
                    set { _byteCodesetCount = value; }
                }
                byte _convertedCodesetCount;

                public byte ConvertedCodesetCount
                {
                    get { return _convertedCodesetCount; }
                    set { _convertedCodesetCount = value; }
                }

                public void ConvertToByte()
                {

                    ByteBrandIndex = Conversion.ConvertToByte(BrandIndex, ByteBrandIndex.Length);
                    ByteCodesetCount = (byte)CodesetCount;
                }

                public void ConvertToLittleEndian()
                {

                    ConvertedBrandIndex = Conversion.ConvertTo2ByteLittleEndian(BrandIndex);
                    ConvertedCodesetCount = (byte)CodesetCount;
                }

                public List<byte> GetByteData()
                {
                    List<byte> byteData = new List<byte>();



                    foreach (byte data in ByteBrandIndex)
                    {
                        byteData.Add(data);
                    }

                    byteData.Add(ByteCodesetCount);

                    return byteData;
                }

                public List<byte> GetConvertedByteData()
                {
                    List<byte> byteData = new List<byte>();



                    foreach (byte data in ConvertedBrandIndex)
                    {
                        byteData.Add(data);
                    }

                    byteData.Add(ConvertedCodesetCount);

                    return byteData;
                }

                #region For Top N Providers With No Zip codes

                    List<BrandIds> _brandIdCollection;
                    public List<BrandIds> BrandIdCollection
                    {
                        get { return _brandIdCollection; }
                        set { _brandIdCollection = value; }
                    }

                    public class BrandIds
                    {
                        int _brandIndex;

                        public int BrandIndex
                        {
                            get { return _brandIndex; }
                            set { _brandIndex = value; }
                        }
                        int _codesetCount;

                        public int CodesetCount
                        {
                            get { return _codesetCount; }
                            set { _codesetCount = value; }
                        }
                        List<string> _codesets;

                        public List<string> Codesets
                        {
                            get { return _codesets; }
                            set { _codesets = value; }
                        }
                        byte[] _byteBrandIndex;

                        public byte[] ByteBrandIndex
                        {
                            get { return _byteBrandIndex; }
                            set { _byteBrandIndex = value; }
                        }

                        byte[] _byteCodesetCount;

                        public byte[] ByteCodesetCount
                        {
                            get { return _byteCodesetCount; }
                            set { _byteCodesetCount = value; }
                        }

                        List<byte[]> _byteCodesets;

                        public List<byte[]> ByteCodesets
                        {
                            get { return _byteCodesets; }
                            set { _byteCodesets = value; }
                        }
                        byte[] _convertedBrandIndex;

                        byte[] _convertedCodesetCount;

                        public byte[] ConvertedCodesetCount
                        {
                            get { return _convertedCodesetCount; }
                            set { _convertedCodesetCount = value; }
                        }

                        public byte[] ConvertedBrandIndex
                        {
                            get { return _convertedBrandIndex; }
                            set { _convertedBrandIndex = value; }
                        }
                        List<byte[]> _convertedCodesets;

                        public List<byte[]> ConvertedCodesets
                        {
                            get { return _convertedCodesets; }
                            set { _convertedCodesets = value; }
                        }

                        public BrandIds()
                        {
                            _brandIndex = 0;
                            _codesetCount = 0;
                            _codesets = new List<string>();


                            ByteBrandIndex = new byte[2];
                            ByteCodesetCount = new byte[1];
                            ByteCodesets = new List<byte[]>();//Each Id to have 3 bytes each


                            ConvertedBrandIndex = new byte[2];
                            ConvertedCodesetCount = new byte[1];
                            ConvertedCodesets = new List<byte[]>();//Each Id to have 3 bytes each
                        }

                        public void ConvertToByte()
                        {
                            ByteBrandIndex = Conversion.ConvertToByte(BrandIndex, ByteBrandIndex.Length);
                            ByteCodesetCount = Conversion.ConvertToByte(CodesetCount, ByteCodesetCount.Length);
                            byte[] _byteCs = new byte[3];
                            foreach (string cs in Codesets)
                            {
                                _byteCs = new byte[3];
                                string _mode = cs.Substring(0, 1);
                                byte[] _byteMode = Conversion.ConvertToByte(_mode, _mode.Length);
                                string _setupcode = cs.Substring(1, 4);
                                byte[] _byteSetupCode = Conversion.ConvertToByte(int.Parse(_setupcode), 2);
                                byte[] _convertedByte = new byte[3];
                                _convertedByte[0] = _byteMode[0];
                                _convertedByte[1] = _byteSetupCode[0];
                                _convertedByte[2] = _byteSetupCode[1];
                                //_byteCs = Conversion.ConvertToByte(cs, _byteCs.Length);//T2051 T uses 1 byte and 2051 uses other 2 bytes
                                ByteCodesets.Add(_convertedByte);

                            }
                        }

                        public void ConvertToEndian(ByteType _type)
                        {
                            switch (_type)
                            {
                                case ByteType.LittleEndian:
                                    {

                                        ConvertedBrandIndex = Conversion.ConvertToLittleEndian(ByteBrandIndex, ByteBrandIndex.Length);
                                        ConvertedCodesetCount = Conversion.ConvertToLittleEndian(ByteCodesetCount, ByteCodesetCount.Length);
                                        byte[] _byteCs = new byte[3];
                                        foreach (byte[] cs in ByteCodesets)
                                        {
                                            _byteCs = new byte[3];
                                            _byteCs = Conversion.ConvertToLittleEndian(cs, 3);
                                            ConvertedCodesets.Add(_byteCs);

                                        }

                                        break;
                                    }
                                case ByteType.BigEndian:
                                    {

                                        break;
                                    }
                            }
                        }

                        public List<byte> GetByteData()
                        {
                            List<byte> _byteData = new List<byte>();

                            foreach (byte by in ByteBrandIndex)
                            {
                                _byteData.Add(by);
                            }
                            foreach (byte by in ByteCodesetCount)
                            {
                                _byteData.Add(by);
                            }
                            foreach (byte[] by in ByteCodesets)
                            {
                                foreach (byte b in by)
                                {
                                    _byteData.Add(b);
                                }
                            }

                            return _byteData;
                        }

                        public List<byte> GetConvertedData()
                        {
                            List<byte> _byteData = new List<byte>();

                            foreach (byte by in ConvertedBrandIndex)
                            {
                                _byteData.Add(by);
                            }
                            foreach (byte by in ConvertedCodesetCount)
                            {
                                _byteData.Add(by);
                            }
                            foreach (byte[] by in ConvertedCodesets)
                            {
                                foreach (byte b in by)
                                {
                                    _byteData.Add(b);
                                }
                            }

                            return _byteData;
                        }

                    }

                #endregion

            }
        }
        public class ProviderLocationCollection
        {
            public ProviderLocationCollection()
            {
                ProviderList = new List<ProviderLocationRecord>();
            }

            List<ProviderLocationRecord> _providerList;

            public List<ProviderLocationRecord> ProviderList
            {
                get { return _providerList; }
                set { _providerList = value; }
            }

            public List<byte> GetByteData()
            {
                List<byte> byteData = new List<byte>();
                foreach (ProviderLocationRecord _provRec in ProviderList)
                {
                    List<byte> _temp = new List<byte>();
                    _temp = _provRec.GetByteData();
                    foreach (byte data in _temp)
                    {
                        byteData.Add(data);
                    }
                }

                return byteData;
            }

            public List<byte> GetConvertedData()
            {
                List<byte> byteData = new List<byte>();
                foreach (ProviderLocationRecord _provRec in ProviderList)
                {
                    List<byte> _temp = new List<byte>();
                    _temp = _provRec.GetConvertedByteData();
                    foreach (byte data in _temp)
                    {
                        byteData.Add(data);
                    }
                }

                return byteData;
            }


        }
        

        #endregion

        #region For Top N Providers with No Zip Codes
        public class ProviderLocationHashCollectionsWithNoZipCodes
        {
            List<ProviderLocationHashDataNoZipCodes> _providerHashCollection;
            public List<ProviderLocationHashDataNoZipCodes> ProviderHashCollection
            {
                get { return _providerHashCollection; }
                set { _providerHashCollection = value; }
            }

            public ProviderLocationHashCollectionsWithNoZipCodes()
            {
                _providerHashCollection = new List<ProviderLocationHashDataNoZipCodes>();
            }
        }
        public class ProviderLocationHashDataNoZipCodes
        {
            string _providerName;

            public string ProviderName
            {
                get { return _providerName; }
                set { _providerName = value; }
            }
            public ProviderLocationHashDataNoZipCodes()
            {
                _locationHashCount = 0;
                _locationHashCollection = new List<ProviderLocationHash>();
                _providerCollectionsWithNoZipCodes = new ProviderLocationCollectionNoZipCodes();

                _byteLocationHashCount = new byte[4];
                _convertedLocationHashCount = new byte[4];

                _providerName = string.Empty;
            }
            int _locationHashCount;

            public int LocationHashCount
            {
                get { return _locationHashCount; }
                set { _locationHashCount = value; }
            }
            byte[] _byteLocationHashCount;

            public byte[] ByteLocationHashCount
            {
                get { return _byteLocationHashCount; }
                set { _byteLocationHashCount = value; }
            }
            byte[] _convertedLocationHashCount;

            public byte[] ConvertedLocationHashCount
            {
                get { return _convertedLocationHashCount; }
                set { _convertedLocationHashCount = value; }
            }

            List<ProviderLocationHash> _locationHashCollection;

            public List<ProviderLocationHash> LocationHashCollection
            {
                get { return _locationHashCollection; }
                set { _locationHashCollection = value; }
            }

            ProviderLocationCollectionNoZipCodes _providerCollectionsWithNoZipCodes;

            public ProviderLocationCollectionNoZipCodes ProviderCollectionsWithNoZipCodes
            {
                get { return _providerCollectionsWithNoZipCodes; }
                set { _providerCollectionsWithNoZipCodes = value; }
            }

            public void ConvertToByte()
            {
                ByteLocationHashCount = Conversion.ConvertToByte(_locationHashCount, ByteLocationHashCount.Length);
            }

            public void ConvertToLittleEndian()
            {
                ConvertedLocationHashCount = Conversion.ConvertTo4ByteLittleEndian(_locationHashCount);
            }

            public List<byte> GetByteData()
            {
                List<byte> byteData = new List<byte>();
                foreach (byte data in ByteLocationHashCount)
                {
                    byteData.Add(data);
                }

                foreach (ProviderLocationHash _lochash in LocationHashCollection)
                {
                    List<byte> _temp = new List<byte>();

                    _temp = _lochash.GetByteData();
                    foreach (byte data in _temp)
                    {
                        byteData.Add(data);
                    }
                }


                return byteData;
            }

            public List<byte> GetConvertedByteData()
            {
                List<byte> byteData = new List<byte>();
                foreach (byte data in ConvertedLocationHashCount)
                {
                    byteData.Add(data);
                }

                foreach (ProviderLocationHash _lochash in LocationHashCollection)
                {
                    List<byte> _temp = new List<byte>();

                    _temp = _lochash.GetConvertedByteData();
                    foreach (byte data in _temp)
                    {
                        byteData.Add(data);
                    }
                }


                return byteData;
            }

            public void AssignLocationHashData(Dictionary<string, int> _locationhashData)
            {
                int counter = 0;
                foreach (KeyValuePair<string, int> _hashdata in _locationhashData)
                {

                    LocationHashCollection[counter].LocationHashValues = _hashdata.Key;
                    LocationHashCollection[counter].LocationHashOffset = _hashdata.Value;
                    LocationHashCollection[counter].ConvertToByte();
                    LocationHashCollection[counter].ConvertToLittleEndian();
                    LocationHashCollection[counter].GetByteData();
                    LocationHashCollection[counter].GetConvertedByteData();
                    counter++;
                }
            }

            public class ProviderLocationHash
            {
                public ProviderLocationHash()
                {
                    _locationHashValues = string.Empty;
                    _locationHashOffset = 0;
                    _byteLocationHashOffset = new byte[4];
                    _byteLocationHashValue = new byte[4];
                    _convertedLocationHashOffset = new byte[4];
                    _convertedLocationHashValue = new byte[4];
                }

                string _locationHashValues;

                public string LocationHashValues
                {
                    get { return _locationHashValues; }
                    set { _locationHashValues = value; }
                }
                byte[] _byteLocationHashValue;

                public byte[] ByteLocationHashValue
                {
                    get { return _byteLocationHashValue; }
                    set { _byteLocationHashValue = value; }
                }
                byte[] _convertedLocationHashValue;

                public byte[] ConvertedLocationHashValue
                {
                    get { return _convertedLocationHashValue; }
                    set { _convertedLocationHashValue = value; }
                }

                int _locationHashOffset;

                public int LocationHashOffset
                {
                    get { return _locationHashOffset; }
                    set { _locationHashOffset = value; }
                }
                byte[] _byteLocationHashOffset;

                public byte[] ByteLocationHashOffset
                {
                    get { return _byteLocationHashOffset; }
                    set { _byteLocationHashOffset = value; }
                }
                byte[] _convertedLocationHashOffset;

                public byte[] ConvertedLocationHashOffset
                {
                    get { return _convertedLocationHashOffset; }
                    set { _convertedLocationHashOffset = value; }
                }

                public void ConvertToByte()
                {
                    ByteLocationHashValue = Conversion.ConvertUTF16ToUTF8(LocationHashValues);
                    ByteLocationHashOffset = Conversion.ConvertToByte(LocationHashOffset, ByteLocationHashOffset.Length);
                }

                public void ConvertToLittleEndian()
                {
                    ConvertedLocationHashValue = Conversion.ConvertUTF16ToUTF8(LocationHashValues);
                    ConvertedLocationHashOffset = Conversion.ConvertTo4ByteLittleEndian(LocationHashOffset);
                }

                public List<byte> GetByteData()
                {
                    List<byte> byteData = new List<byte>();

                    foreach (byte data in ByteLocationHashValue)
                    {
                        byteData.Add(data);
                    }

                    foreach (byte data in ByteLocationHashOffset)
                    {
                        byteData.Add(data);
                    }

                    return byteData;
                }

                public List<byte> GetConvertedByteData()
                {
                    List<byte> byteData = new List<byte>();

                    foreach (byte data in ConvertedLocationHashValue)
                    {
                        byteData.Add(data);
                    }

                    foreach (byte data in ConvertedLocationHashOffset)
                    {
                        byteData.Add(data);
                    }

                    return byteData;
                }
            }

        }
        public class ProviderLocationRecordNoZipCodes
        {
            public ProviderLocationRecordNoZipCodes()
            {
                LocationData = string.Empty;
                ByteLocationData = new byte[4];
                ConvertedLocationData = new byte[4];

                _brandCount = 0;
                _byteBrandCount = new byte();
                _convertedBrandCount = new byte();
            }

            public void ConvertToByte()
            {
                //Assuming that the bit 7 of byte 0 is 0 for now, needs to be changed later

                ByteLocationData = Conversion.ConvertUTF16ToUTF8(LocationData);
                ByteBrandCount = (byte)BrandCount;
            }
            public void ConvertToLittleEndian()
            {
                ConvertedLocationData = Conversion.ConvertUTF16ToUTF8(LocationData);
                ConvertedBrandCount = (byte)BrandCount;
            }

            byte _byte0Data = 0x00;

            string _locationData;
            public string LocationData
            {
                get { return _locationData; }
                set { _locationData = value; }
            }
            byte[] _byteLocationData;
            public byte[] ByteLocationData
            {
                get { return _byteLocationData; }
                set { _byteLocationData = value; }
            }

            byte[] _convertedLocationData;
            public byte[] ConvertedLocationData
            {
                get { return _convertedLocationData; }
                set { _convertedLocationData = value; }
            }

            List<LocationRecords> _providerLocRec;
            public List<LocationRecords> ProviderLocRec
            {
                get { return _providerLocRec; }
                set { _providerLocRec = value; }
            }

            int _brandCount = 0;
            public int BrandCount
            {
                get { return _brandCount; }
                set { _brandCount = value; }
            }
            byte _byteBrandCount;
            public byte ByteBrandCount
            {
                get { return _byteBrandCount; }
                set { _byteBrandCount = value; }
            }
            byte _convertedBrandCount = 0;
            public byte ConvertedBrandCount
            {
                get { return _convertedBrandCount; }
                set { _convertedBrandCount = value; }
            }



            public List<byte> GetByteData()
            {
                List<byte> byteData = new List<byte>();

                foreach (byte data in ByteLocationData)
                {
                    byteData.Add(data);
                }

                byteData.Add(ByteBrandCount);


                List<byte> _temp = new List<byte>();

                foreach (LocationRecords _locrec in ProviderLocRec)
                {
                    _temp = new List<byte>();
                    _temp = _locrec.GetByteData();
                    foreach (byte data in _temp)
                    {
                        byteData.Add(data);
                    }
                }

                return byteData;
            }

            public List<byte> GetConvertedByteData()
            {
                List<byte> byteData = new List<byte>();

                foreach (byte data in ConvertedLocationData)
                {
                    byteData.Add(data);
                }

                byteData.Add(ConvertedBrandCount);

                List<byte> _temp = new List<byte>();

                foreach (LocationRecords _locrec in ProviderLocRec)
                {
                    _temp = new List<byte>();
                    _temp = _locrec.GetConvertedByteData();
                    foreach (byte data in _temp)
                    {
                        byteData.Add(data);
                    }
                }

                return byteData;
            }

            public class LocationRecords
            {
                public LocationRecords()
                {

                    _brandIndex = 0;
                    _byteBrandIndex = new byte[2];
                    _convertedBrandIndex = new byte[2];
                    ByteCodesets = new List<byte[]>();//Each Id to have 3 bytes each

                    _codesetCount = 0;
                    _byteCodesetCount = new byte();
                    _convertedCodesetCount = new byte();
                    ConvertedCodesets = new List<byte[]>();//Each Id to have 3 bytes each
                    
                }


                int _brandIndex = 0;

                public int BrandIndex
                {
                    get { return _brandIndex; }
                    set { _brandIndex = value; }
                }
                byte[] _byteBrandIndex;

                public byte[] ByteBrandIndex
                {
                    get { return _byteBrandIndex; }
                    set { _byteBrandIndex = value; }
                }
                byte[] _convertedBrandIndex;

                public byte[] ConvertedBrandIndex
                {
                    get { return _convertedBrandIndex; }
                    set { _convertedBrandIndex = value; }
                }

                int _codesetCount = 0;

                public int CodesetCount
                {
                    get { return _codesetCount; }
                    set { _codesetCount = value; }
                }
                byte _byteCodesetCount;

                public byte ByteCodesetCount
                {
                    get { return _byteCodesetCount; }
                    set { _byteCodesetCount = value; }
                }
                byte _convertedCodesetCount;

                public byte ConvertedCodesetCount
                {
                    get { return _convertedCodesetCount; }
                    set { _convertedCodesetCount = value; }
                }

                public void ConvertToByte()
                {

                    ByteBrandIndex = Conversion.ConvertToByte(BrandIndex, ByteBrandIndex.Length);
                    ByteCodesetCount = (byte)CodesetCount;
                    byte[] _byteCs = new byte[3];
                    foreach (string cs in Codesets)
                    {
                        _byteCs = new byte[3];
                        string _mode = cs.Trim().Substring(0, 1);
                        byte[] _byteMode = Conversion.ConvertToByte(_mode, _mode.Length);
                        string _setupcode = cs.Trim().Substring(1, 4);
                        byte[] _byteSetupCode = Conversion.ConvertToByte(int.Parse(_setupcode), 2);
                        byte[] _convertedByte = new byte[3];
                        _convertedByte[0] = _byteMode[0];
                        _convertedByte[1] = _byteSetupCode[0];
                        _convertedByte[2] = _byteSetupCode[1];
                        //_byteCs = Conversion.ConvertToByte(cs, _byteCs.Length);//T2051 T uses 1 byte and 2051 uses other 2 bytes
                        ByteCodesets.Add(_convertedByte);

                    }
                }

                public void ConvertToLittleEndian()
                {

                    ConvertedBrandIndex = Conversion.ConvertTo2ByteLittleEndian(BrandIndex);
                    ConvertedCodesetCount = (byte)CodesetCount;
                    byte[] _byteCs = new byte[3];
                    foreach (byte[] cs in ByteCodesets)
                    {
                        _byteCs = new byte[3];
                        _byteCs = Conversion.ConvertToLittleEndian(cs, 3);
                        ConvertedCodesets.Add(_byteCs);

                    }
                }

                public List<byte> GetByteData()
                {
                    List<byte> byteData = new List<byte>();

                    foreach (byte data in ByteBrandIndex)
                    {
                        byteData.Add(data);
                    }

                    byteData.Add(ByteCodesetCount);

                    foreach (byte[] by in ByteCodesets)
                    {
                        foreach (byte b in by)
                        {
                            byteData.Add(b);
                        }
                    }
                                        

                    return byteData;
                }

                public List<byte> GetConvertedByteData()
                {
                    List<byte> byteData = new List<byte>();



                    foreach (byte data in ConvertedBrandIndex)
                    {
                        byteData.Add(data);
                    }

                    byteData.Add(ConvertedCodesetCount);

                    foreach (byte[] by in ConvertedCodesets)
                    {
                        foreach (byte b in by)
                        {
                            byteData.Add(b);
                        }
                    }

                    return byteData;
                }

                #region For Top N Providers With No Zip codes

                List<BrandIds> _brandIdCollection;
                public List<BrandIds> BrandIdCollection
                {
                    get { return _brandIdCollection; }
                    set { _brandIdCollection = value; }
                }

                //Added to handle the Top N without Zip Codes

                List<string> _codesets;

                public List<string> Codesets
                {
                    get { return _codesets; }
                    set { _codesets = value; }
                }
                List<byte[]> _byteCodesets;

                public List<byte[]> ByteCodesets
                {
                    get { return _byteCodesets; }
                    set { _byteCodesets = value; }
                }

                List<byte[]> _convertedCodesets;

                public List<byte[]> ConvertedCodesets
                {
                    get { return _convertedCodesets; }
                    set { _convertedCodesets = value; }
                }
                // End comment

                public class BrandIds
                {
                    int _brandIndex;

                    public int BrandIndex
                    {
                        get { return _brandIndex; }
                        set { _brandIndex = value; }
                    }
                    int _codesetCount;

                    public int CodesetCount
                    {
                        get { return _codesetCount; }
                        set { _codesetCount = value; }
                    }
                    List<string> _codesets;

                    public List<string> Codesets
                    {
                        get { return _codesets; }
                        set { _codesets = value; }
                    }
                    List<byte[]> _byteCodesets;

                    public List<byte[]> ByteCodesets
                    {
                        get { return _byteCodesets; }
                        set { _byteCodesets = value; }
                    }

                    byte[] _byteBrandIndex;

                    public byte[] ByteBrandIndex
                    {
                        get { return _byteBrandIndex; }
                        set { _byteBrandIndex = value; }
                    }

                    byte[] _byteCodesetCount;

                    public byte[] ByteCodesetCount
                    {
                        get { return _byteCodesetCount; }
                        set { _byteCodesetCount = value; }
                    }

                   
                    byte[] _convertedBrandIndex;

                    byte[] _convertedCodesetCount;

                    public byte[] ConvertedCodesetCount
                    {
                        get { return _convertedCodesetCount; }
                        set { _convertedCodesetCount = value; }
                    }

                    public byte[] ConvertedBrandIndex
                    {
                        get { return _convertedBrandIndex; }
                        set { _convertedBrandIndex = value; }
                    }
                    List<byte[]> _convertedCodesets;

                    public List<byte[]> ConvertedCodesets
                    {
                        get { return _convertedCodesets; }
                        set { _convertedCodesets = value; }
                    }

                    public BrandIds()
                    {
                        _brandIndex = 0;
                        _codesetCount = 0;
                        _codesets = new List<string>();


                        ByteBrandIndex = new byte[2];
                        ByteCodesetCount = new byte[1];
                        ByteCodesets = new List<byte[]>();//Each Id to have 3 bytes each


                        ConvertedBrandIndex = new byte[2];
                        ConvertedCodesetCount = new byte[1];
                        ConvertedCodesets = new List<byte[]>();//Each Id to have 3 bytes each
                    }

                    public void ConvertToByte()
                    {
                        ByteBrandIndex = Conversion.ConvertToByte(BrandIndex, ByteBrandIndex.Length);
                        ByteCodesetCount = Conversion.ConvertToByte(CodesetCount, ByteCodesetCount.Length);
                        byte[] _byteCs = new byte[3];
                        foreach (string cs in Codesets)
                        {
                            _byteCs = new byte[3];
                            string _mode = cs.Substring(0, 1);
                            byte[] _byteMode = Conversion.ConvertToByte(_mode, _mode.Length);
                            string _setupcode = cs.Substring(1, 4);
                            byte[] _byteSetupCode = Conversion.ConvertToByte(int.Parse(_setupcode), 2);
                            byte[] _convertedByte = new byte[3];
                            _convertedByte[0] = _byteMode[0];
                            _convertedByte[1] = _byteSetupCode[0];
                            _convertedByte[2] = _byteSetupCode[1];
                            //_byteCs = Conversion.ConvertToByte(cs, _byteCs.Length);//T2051 T uses 1 byte and 2051 uses other 2 bytes
                            ByteCodesets.Add(_convertedByte);

                        }
                    }

                    public void ConvertToEndian(ByteType _type)
                    {
                        switch (_type)
                        {
                            case ByteType.LittleEndian:
                                {

                                    ConvertedBrandIndex = Conversion.ConvertToLittleEndian(ByteBrandIndex, ByteBrandIndex.Length);
                                    ConvertedCodesetCount = Conversion.ConvertToLittleEndian(ByteCodesetCount, ByteCodesetCount.Length);
                                    byte[] _byteCs = new byte[3];
                                    foreach (byte[] cs in ByteCodesets)
                                    {
                                        _byteCs = new byte[3];
                                        _byteCs = Conversion.ConvertToLittleEndian(cs, 3);
                                        ConvertedCodesets.Add(_byteCs);

                                    }

                                    break;
                                }
                            case ByteType.BigEndian:
                                {

                                    break;
                                }
                        }
                    }

                    public List<byte> GetByteData()
                    {
                        List<byte> _byteData = new List<byte>();

                        foreach (byte by in ByteBrandIndex)
                        {
                            _byteData.Add(by);
                        }
                        foreach (byte by in ByteCodesetCount)
                        {
                            _byteData.Add(by);
                        }
                        foreach (byte[] by in ByteCodesets)
                        {
                            foreach (byte b in by)
                            {
                                _byteData.Add(b);
                            }
                        }

                        return _byteData;
                    }

                    public List<byte> GetConvertedData()
                    {
                        List<byte> _byteData = new List<byte>();

                        foreach (byte by in ConvertedBrandIndex)
                        {
                            _byteData.Add(by);
                        }
                        foreach (byte by in ConvertedCodesetCount)
                        {
                            _byteData.Add(by);
                        }
                        foreach (byte[] by in ConvertedCodesets)
                        {
                            foreach (byte b in by)
                            {
                                _byteData.Add(b);
                            }
                        }

                        return _byteData;
                    }

                }

                #endregion

            }
        }
        public class ProviderLocationCollectionNoZipCodes
        {
            string _providerName;

            public string ProviderName
            {
                get { return _providerName; }
                set { _providerName = value; }
            }
            public ProviderLocationCollectionNoZipCodes()
            {
                ProviderList = new List<ProviderLocationRecordNoZipCodes>();
                _providerName = string.Empty;
            }

            List<ProviderLocationRecordNoZipCodes> _providerList;

            public List<ProviderLocationRecordNoZipCodes> ProviderList
            {
                get { return _providerList; }
                set { _providerList = value; }
            }

            public List<byte> GetByteData()
            {
                List<byte> byteData = new List<byte>();
                foreach (ProviderLocationRecordNoZipCodes _provRec in ProviderList)
                {
                    List<byte> _temp = new List<byte>();
                    _temp = _provRec.GetByteData();
                    foreach (byte data in _temp)
                    {
                        byteData.Add(data);
                    }
                }

                return byteData;
            }

            public List<byte> GetConvertedData()
            {
                List<byte> byteData = new List<byte>();
                foreach (ProviderLocationRecordNoZipCodes _provRec in ProviderList)
                {
                    List<byte> _temp = new List<byte>();
                    _temp = _provRec.GetConvertedByteData();
                    foreach (byte data in _temp)
                    {
                        byteData.Add(data);
                    }
                }

                return byteData;
            }


        }

        #endregion

    }
    
    public enum ByteType
    {
        LittleEndian = 0,
        BigEndian
    };

    #region Data structure for Location record with no zip codes
    public class LocationRecord_NoZipCode
    {
        List<Devices> _deviceList;

        public List<Devices> DeviceList
        {
            get { return _deviceList; }
            set { _deviceList = value; }
        }
        public LocationRecord_NoZipCode()
        {
            _deviceList = new List<Devices>();
        }
    }

    public class Devices
    {
        string _name;

        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }
        List<Locations> _location;

        public List<Locations> Location
        {
            get { return _location; }
            set { _location = value; }
        }

        string _deviceletters;

        public string Deviceletters
        {
            get { return _deviceletters; }
            set { _deviceletters = value; }
        }

        public Devices()
        {
            _name = string.Empty;
            _location = new List<Locations>();
            _deviceletters = string.Empty;
        }
    }

    public class Locations
    {
        string _region;

        public string Region
        {
            get { return _region; }
            set { _region = value; }
        }
        string _country;

        public string Country
        {
            get { return _country; }
            set { _country = value; }
        }
        List<Brands> _brandlist;

        public List<Brands> Brandlist
        {
            get { return _brandlist; }
            set { _brandlist = value; }
        }

        public Locations()
        {
            _region = string.Empty;
            _country = string.Empty;
            _brandlist = new List<Brands>();
        }
    }

    public class Brands
    {
        string _name;

        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }
        List<string> _idlist;

        public List<string> Idlist
        {
            get { return _idlist; }
            set { _idlist = value; }
        }

        public Brands()
        {
            _name = string.Empty;
            _idlist = new List<string>();
        }
    }
    #endregion

    #region Type Records Settings
    public enum TypeRecSettings
    {
        ZipCodes=0,
        TopNWithBrandId,
        TopNOnly,
        TopNManufacturers,
        ProvinceAndCity
    };
    #endregion

    #region Province City Data structure
    public class PCTypeLocationHashData
    {
        byte[] _countrycode;

        public byte[] Countrycode
        {
            get { return _countrycode; }
            set { _countrycode = value; }
        }
        byte[] _provincecode;

        public byte[] Provincecode
        {
            get { return _provincecode; }
            set { _provincecode = value; }
        }
        byte[] _fileOffset;

        public byte[] FileOffset
        {
            get { return _fileOffset; }
            set { _fileOffset = value; }
        }
    }

    public class PCLocationData
    {
        byte[] _locationdata;

        public byte[] Locationdata
        {
            get { return _locationdata; }
            set { _locationdata = value; }
        }
        //byte[] _province;
        //public byte[] Province
        //{
        //    get { return _province; }
        //    set { _province = value; }
        //}

        //byte[] _city;

        //public byte[] City
        //{
        //    get { return _city; }
        //    set { _city = value; }
        //}

        byte[] _brandcount;
        public byte[] Brandcount
        {
            get { return _brandcount; }
            set { _brandcount = value; }
        }
        List<PCBrandIdCollection> _brandIdCollection;
        public List<PCBrandIdCollection> BrandIdCollection
        {
            get { return _brandIdCollection; }
            set { _brandIdCollection = value; }
        }

        public PCLocationData()
        {
            //_province = new byte[2];
            //_city = new byte[2];
            _locationdata = new byte[4];
            _brandcount = new byte[2];
            _brandIdCollection = new List<PCBrandIdCollection>();
        }
    }
    public class PCBrandIdCollection
    {
        byte[] _brandindex;
        public byte[] Brandindex
        {
            get { return _brandindex; }
            set { _brandindex = value; }
        }
        byte[] _cscount;
        public byte[] Cscount
        {
            get { return _cscount; }
            set { _cscount = value; }
        }
        byte[] _cslist;
        public byte[] Cslist
        {
            get { return _cslist; }
            set { _cslist = value; }
        }

        public PCBrandIdCollection()
        {
            Brandindex = new byte[2];
            Cscount = new byte[2];

        }

    }
   #endregion



}
